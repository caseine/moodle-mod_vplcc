<?php
// This file is part of VPL Code Challenges for Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * @author Photis Avrilionis
 * @copyright 2020 Photis Avrilionis
 *
 * This software was developed with the support of the following organizations:
 * - Université Grenoble Alpes
 * - Institut Polytechnique de Grenoble
 */

namespace mod_vplcc\controllers;

defined('MOODLE_INTERNAL') || die();

use core\output\notification;
use moodle_url;
use user_picture;
use context_module;
use mod_vpl;
use mod_vplcc\core\http\request;
use mod_vplcc\core\routing\url;
use mod_vplcc\core\renderables\page;
use mod_vplcc\models\evaluation_fact;
use mod_vplcc\models\grid;
use mod_vplcc\models\dimension;
use mod_vplcc\models\vplcc;
use mod_vplcc\resources\leaderboard_grid_resource;
use mod_vplcc\resources\pagination_resource;
use mod_vplcc\factories\evaluations_factory;

class leaderboard extends base_controller {

    /**
     * Displays a leaderboard for the attached code challenge activity.
     *
     * @todo better handling of missing VPL activity.
     *
     * @return page
     */
    public function view() {
        global $PAGE, $DB, $USER;

        $id = required_param('id', PARAM_INT);

        if (! $cm = get_coursemodule_from_id('vplcc', $id)) {
            print_error('invalidcoursemodule');
        }

        if (! $course = $DB->get_record("course", ["id" => $cm->course])) {
            print_error('coursemisconf');
        }

        require_login($course, true, $cm);

        if (! $vplcc = vplcc::find($cm->instance)) {
            print_error('invalidcoursemodule');
        }

        if (! $vpl = new mod_vpl($vplcc->vplcm)) {
            print_error('invalidcoursemodule');
        }

        if (! $vplcm = $vpl->get_course_module()) {
            print_error('invalidcoursemodule');
        }

        $PAGE->set_context(context_module::instance($cm->id));
        $PAGE->set_pagelayout('incourse');

        $PAGE->set_title($vplcc->name);
        $PAGE->set_heading($vplcc->name);

        $groupactivity = $vpl->is_group_activity();

        // Calls ETL process to load new VPL submissions.
        evaluations_factory::prepare_leaderboard($vplcc, $vpl);

        // Leaderboard grid definition.
        $grid = new grid();

        $grid->set_dimension('identifier', $groupactivity ? get_string('leaderboard:list:group', 'vplcc') : get_string('leaderboard:list:name', 'vplcc'));
        $grid->set_dimension('grade', get_string('leaderboard:list:grade', 'vplcc'));
        $grid->set_dimension('picture', "");

        $dimensions = dimension::all([
            'vplccid' => $vplcc->id,
        ]);

        $hasdimensions = $dimensions !== false;

        if ($hasdimensions) {
            foreach ($dimensions as $key) {
                $grid->set_dimension($key->slug, $key->name, $key->description);
            }
        }

        $grid->set_columns($vplcc->get_columns());
        $grid->set_metadata($vplcc->get_metadata());

        // Leaderboard grid population.
        $pageno = request::get_instance()->param('page', PARAM_INT, 1);

        if ($vplcc->scope != vplcc::SCOPE_NONE && ($vpl->has_capability(VPL_MANAGE_CAPABILITY) || $vpl->has_capability(VPL_GRADE_CAPABILITY))) {
            $scopes = groups_get_all_groups($vplcc->course, 0, $vplcc->get_scope_grouping());
            $scope = request::get_instance()->param('scope', PARAM_INT, array_keys($scopes)[0]);
        }

        if ($vplcc->scope == vplcc::SCOPE_NONE || isset($scope) && $scope == 0) {

            if ($groupactivity) {
                list($evaluations, $pagination) = evaluations_factory::get_all_group_evaluations($vplcc, $pageno);
            } else {
                list($evaluations, $pagination) = evaluations_factory::get_all_individual_evaluations($vplcc, $pageno);
            }

        } else {

            if ($vpl->has_capability(VPL_MANAGE_CAPABILITY) || $vpl->has_capability(VPL_GRADE_CAPABILITY)) {

                if (!isset($scopes[$scope])) {
                    print_error('errorinvalidgroup', 'group');
                }

                if ($groupactivity) {
                    list($evaluations, $pagination) = evaluations_factory::get_group_evaluations_for_scope($vplcc, $vpl, $scope, $pageno);
                } else {
                    list($evaluations, $pagination) = evaluations_factory::get_individual_evaluations_for_scope($vplcc, $scope, $pageno);
                }

            } else {

                if (empty(groups_get_all_groups($vplcc->course, $USER->id, $vplcc->get_scope_grouping()))) {
                    redirect(url::route('view', ['id' => $cm->id]), get_string('leaderboard:noncompeting', 'vplcc'), null, notification::NOTIFY_WARNING);
                }

                if ($groupactivity) {
                    list($evaluations, $pagination) = evaluations_factory::get_group_evaluations_for_user($vplcc, $vpl, $USER->id, $pageno);

                } else {
                    list($evaluations, $pagination) = evaluations_factory::get_individual_evaluations_for_user($vplcc, $USER->id, $pageno);
                }
            }
        }

        if (empty($evaluations) && $pagination['total_pages'] > 0) {
            if ($pageno < 1 && $pagination['total_pages'] > 0) {
                redirect(url::route('leaderboard.view', array_merge(request::get_instance()->params(), ['page' => 1])));
            } else if ($pageno > $pagination['total_pages'] && $pagination['total_pages'] > 0) {
                redirect(url::route('leaderboard.view', array_merge(request::get_instance()->params(), ['page' => $pagination['total_pages']])));
            }
        }

        if ($evaluations->valid()) {

            if ($hasdimensions) {
                $dimensionsmap = array_reduce($dimensions, function ($carry, $dimension) {
                    $carry[$dimension->id] = $dimension->slug;
                    return $carry;
                }, []);
            }

            foreach ($evaluations as $evaluation) {

                $entry = [];

                if ($groupactivity) {
                    $group = $DB->get_record("groups", ["id" => $evaluation->groupid]);
                    if ($group !== false) {
                        $entry['identifier'] = $group->name;
                        $entry['picture'] = get_group_picture_url($group, $course->id);
                    }
                } else {
                    // prepare user data
                    $fields = array_merge(['id', 'idnumber', 'picture', 'imagealt', 'email'], get_all_user_name_fields());
                    $user = $DB->get_record("user", ["id" => $evaluation->userid], implode(',', $fields));
                    if ($user !== false) {
                        $entry['identifier'] = isset($user->alternatename) && !empty($user->alternatename) ? $user->alternatename : fullname($user);
                        $entry['picture'] = (new user_picture($user))->get_url($PAGE);
                        $entry['auth_user'] = ($USER->id == $user->id);
                    }
                }

                if ($hasdimensions) {
                    $facts = evaluation_fact::all([
                        'vplccsubmissionid' => $evaluation->id
                    ]);

                    if ($facts !== false) {
                        foreach ($facts as $fact) {
                            if (isset($dimensionsmap[$fact->keyid])) {
                                $entry[$dimensionsmap[$fact->keyid]] = $fact->data;
                            }
                        }
                    }
                }

                $entry['grade'] = number_format($evaluation->grade, 2);

                $grid->add_entry($entry);
            }
        }

        $evaluations->close();

        $selectedscope = isset($scope) ? $scope : 0;

        return new page('mod_vplcc/pages/leaderboard', [
            'feedback' => $this->feedback_data(),
            'navigation' => $this->navigation_data($cm, 'leaderboard'),
            'empty' => $grid->count() == 0,
            'leaderboard' => leaderboard_grid_resource::from($grid, $pagination['offset'])->to_array(),
            'scopes' => [
                'can_switch' => isset($scopes),
                'list' => isset($scopes) ? array_reduce($scopes, function ($carry, $el) use ($selectedscope) {
                    $carry[] = [
                        'id' => $el->id,
                        'name' => $el->name,
                        'selected' => $selectedscope == $el->id,
                    ];
                    return $carry;
                }, [['id' => 0, 'name' => get_string('all'), 'selected' => $selectedscope == 0]]) : false,
            ],
            'pagination' => pagination_resource::from($pagination)->to_array(),
            'countdown' => ($vplcc->starttimestamp > time() && !($vpl->has_capability(VPL_MANAGE_CAPABILITY) || $vpl->has_capability(VPL_GRADE_CAPABILITY))) ? $vplcc->starttimestamp : false,
            'user_settings' => new moodle_url('/user/edit.php', ['id' => $USER->id], 'id_moodle_additional_names'),
        ]);
    }
}
