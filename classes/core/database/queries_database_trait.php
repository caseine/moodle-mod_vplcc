<?php
// This file is part of VPL Code Challenges for Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * @author Photis Avrilionis
 * @copyright 2020 Photis Avrilionis
 *
 * This software was developed with the support of the following organizations:
 * - Université Grenoble Alpes
 * - Institut Polytechnique de Grenoble
 */

namespace mod_vplcc\core\database;

defined('MOODLE_INTERNAL') || die();

use moodle_database;

trait queries_database_trait {

    /**
     * Returns Moodle's global database instance
     * @return moodle_database
     */
    protected static function db() {
        global $DB;
        return $DB;
    }

    protected static function generate_where_list_fragment($field, $list) {
        $count = count($list);
        if ($count == 0) {
            return "0 = 1";
        } else if ($count == 1) {
            return "$field = ?";
        } else {
            $qs = ltrim(str_repeat(',?', $count), ',');
            return "$field IN ($qs)";
        }
    }

}

