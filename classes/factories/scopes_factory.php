<?php
// This file is part of VPL Code Challenges for Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * @author Photis Avrilionis
 * @copyright 2020 Photis Avrilionis
 *
 * This software was developed with the support of the following organizations:
 * - Université Grenoble Alpes
 * - Institut Polytechnique de Grenoble
 */

namespace mod_vplcc\factories;

defined('MOODLE_INTERNAL') || die();

use mod_vpl;
use mod_vplcc\core\database\queries_database_trait;
use mod_vplcc\models\vplcc;

class scopes_factory {

    use queries_database_trait;

    /**
     * Returns an array of user ids that belong to the same scope as the given user.
     *
     * @param vplcc $vplcc
     * @param int $userid
     * @return array
     */
    public static function get_scope_users_for_user($vplcc, $userid) {
        // Retrieve scope grouping
        $grouping = $vplcc->get_scope_grouping();

        if (! empty($grouping)) {

            // Retrieve the groups to which the user belongs in this group
            $groups = groups_get_user_groups($vplcc->course, $userid);

            if (isset($groups[$grouping]) && !empty($groups[$grouping])) {

                // Return the users that belong to those groups
                return groups_get_groups_members($groups[$grouping], null, 'id ASC');
            }
        }

        return [];
    }

    /**
     * Returns an array of user ids that belong to the given scope group.
     *
     * @param $groupid the scope group
     * @return array
     */
    public static function get_scope_users_for_scope($groupid) {
        return groups_get_members($groupid);
    }

    /**
     * Returns an array of all groups that the given users belongs to
     *
     * Simplified version groups_get_all_groups that retrieves distinct groups
     *
     * @param int $courseid
     * @param int $groupingid
     * @param array $users
     * @param string $fields
     * @return array
     */
    protected static function get_all_groups($courseid, $users, $groupingid, $fields='g.*') {

        $params = array_merge([
            $courseid,
            $groupingid,
        ], $users);

        $fragment = self::generate_where_list_fragment('gm.userid', $users);

        $sql = "select distinct $fields
            from {groups} g
            join {groups_members} gm on gm.groupid = g.id
            join {groupings_groups} gg on gg.groupid = g.id
            where g.courseid = ? and
                  gg.groupingid = ? and
                  $fragment
            order by g.name asc";

        return self::db()->get_records_sql($sql, $params);
    }

    /**
     * Returns an array of groups belonging to the user's scope
     *
     * @todo replace with custom SQL query: Moodle does not check for distinct entries
     *
     * @param $vplcc
     * @param mod_vpl $vpl
     * @param $userid
     * @return array
     */
    public static function get_scope_groups_for_user($vplcc, $vpl, $userid) {
        return static::get_all_groups($vplcc->course, array_keys(static::get_scope_users_for_user($vplcc, $userid)), $vpl->get_course_module()->groupingid);
    }

    /**
     * Returns an array of groups belonging to the specified scope
     *
     * @param $vplcc
     * @param $groupid
     * @param $vpl
     * @return array
     */
    public static function get_scope_groups_for_scope($vplcc, $vpl, $groupid) {
        return static::get_all_groups($vplcc->course, array_keys(static::get_scope_users_for_scope($groupid)), $vpl->get_course_module()->groupingid);
    }

}