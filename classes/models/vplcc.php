<?php
// This file is part of VPL Code Challenges for Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * @author Photis Avrilionis
 * @copyright 2020 Photis Avrilionis
 *
 * This software was developed with the support of the following organizations:
 * - Université Grenoble Alpes
 * - Institut Polytechnique de Grenoble
 */

namespace mod_vplcc\models;

defined('MOODLE_INTERNAL') || die();

use mod_vplcc\core\database\model;

/**
 * Class vplcc
 *
 * ORM model for vpl code challenge instances.
 *
 * @property int id the activity id
 * @property int course the course that the vplcc activity belongs to
 * @property string name the vplcc activity's name
 * @property string intro the vplcc activity's raw intro/description text
 * @property int introformat the intro text rendering format
 * @property int starttimestamp
 * @property int endtimestamp
 * @property int vplcm the attached activity course module id
 * @property int scope the leaderboard's scope grouping
 * @property int columns the vplcc's columns dimensions (comma-separated slugs)
 * @property int metadata the vplcc's metadata dimensions (comma-separated slugs)
 * @property int usermodified
 * @property int timecreated
 * @property int timemodified
 *
 * @package mod_vplcc\models
 */
class vplcc extends model {

    const SCOPE_NONE = 0;
    const SCOPE_ATTACHED_GROUPING = -1;

    protected $table = "vplcc";

    protected $attributes = [
        'id',
        'course',

        'name',
        'intro',
        'introformat',
        'starttimestamp',
        'endtimestamp',
        'vplcm',
        'scope',

        'columns',
        'metadata',

        'usermodified',
        'timecreated',
        'timemodified'
    ];

    protected $fillable = [
        'name',
        'course',
        'description',
        'descriptionformat',
        'starttimestamp',
        'endtimestamp',
        'vplcm',
        'columns',
        'metadata',
    ];

    protected $dimdelimeter = ',';

    /**
     * Returns the vplcc's columns dimensions as an array
     *
     * @return false|string[]
     */
    public function get_columns() {
        return array_filter(explode($this->dimdelimeter, $this->columns));
    }

    /**
     * Sets the vplcc's columns dimensions from an array
     *
     * @param $columns
     * @return $this
     */
    public function set_columns($columns) {
        $this->columns = join($this->dimdelimeter, $columns);
        return $this;
    }

    /**
     * Returns the vplcc's metadata dimensions as an array
     *
     * @return false|string[]
     */
    public function get_metadata() {
        return array_filter(explode($this->dimdelimeter, $this->metadata));
    }

    /**
     * Sets the vplcc's metadata dimensions from an array
     *
     * @param $metadata
     * @return $this
     */
    public function set_metadata($metadata) {
        $this->metadata = join($this->dimdelimeter, $metadata);
        return $this;
    }

    /**
     * Returns the scope restriction grouping id, false if no restrictions.
     *
     * @return bool|int
     */
    public function get_scope_grouping() {
        if ($this->scope == static::SCOPE_NONE) {
            return false;
        }

        if ($this->scope == static::SCOPE_ATTACHED_GROUPING) {
            $grouping = get_coursemodule_from_id('vpl', $this->vplcm)->groupingid;

            if ($grouping == 0) {
                return false;
            }

            return $grouping;
        }

        return $this->scope;
    }

}
