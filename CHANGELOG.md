# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

Official releases are available for download at https://gricad-gitlab.univ-grenoble-alpes.fr/avriliop/caseine-code-challenges/-/releases

## v0.7.0 - 2020-07-24
### Added
- Added a configuration option that allows code challenge creators to separate challenge takers in distinct scopes (configured using groupings). Relates to issue #25.
- Added a link to the attached activity in code challenges summary blocks.
- French translations 🇫🇷

### Fixed
- Fixed issue with code challenges and VPL activities in group mode. This caused issues when rendering leaderboards for some configurations.
- Leaderboard paginator now displays the active page.
- Fixed various object access issues
- Fixed issue with VPL grouped evaluation mode. Fixes #25.
- Fixed an issue that caused users to see a random group in the VPL activity grouping if they did not belong to one.
- Fixed an issue that made the leaderboard unavailable even after the release date.
- Implemented Moodle PHP code style in most plugin files.

## v0.6.5 - 2020-07-17
### Changed
- Modified the VPL execution file parser to recognize any string as a value of a leaderboard key. Previous versions only accepted integers as the value of a key.
- Restricted leaderboards to exclude users that can _grade_ or _manage_ VPL activities. This includes all users with at least one of the following roles on a course (with exclusions): teacher, non-editing teacher, course creator or manager. For more details, please [refer to this article](https://pavril.github.io/vpl-code-challenges-docs/#/advanced/miscellaneous?id=non-competing-users). Relates to issue #29

### Fixed
- Fixed issue where ties in the leaderboard were randomly reordered each time the page was refreshed. From now on, ties are will be resolved by sorting the tied contestants by the date they received their grade: the first one to receive the tie grade, will be placed higher in the leaderboard. Relates to issue #19.
- Fixed issue that prevented VPL evaluations from being stored in the database if no code challenge was attached to them. Relates to issue #26.

## v0.6.1 - 2020-07-13
### Added
- Leaderboard announcement date option. Allows teachers to set a date before which the leaderboard is not accessible to students.

### Changed
- Redesigned the leaderboard metadata modal to better accommodate large amounts of metadata dimensions.
- Replaced the feedback popup documentation link to point to the new wiki website.

### Fixed
- Fixed issue where function would try to loop through dimensions even if none were set when updating a VPL code challenge.
- Fixed issue where leaderboard evaluation data equal to `0` would be treated as missing data.
- Issue when reading latest update date.

## v0.5.0 - 2020-07-12
### Added
- Implemented pagination for leaderboard views.
- Added option to hide feedback modal from the code challenge pages.

## v0.4.3 - 2020-07-11
### Fixed
- `setType()` not defined for mod_form fields (issue [#20](https://gricad-gitlab.univ-grenoble-alpes.fr/avriliop/caseine-code-challenges/-/issues/20)).
- Database migration issues (faulty database definition in install.xml, issue [#21](https://gricad-gitlab.univ-grenoble-alpes.fr/avriliop/caseine-code-challenges/-/issues/21)).
- Unused vplcc object attributes causing PHP warning when updating code challenge activities.