<?php
// This file is part of VPL Code Challenges for Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Add VPL Code Challenge form
 *
 * @package mod_vplcc
 * @copyright TODO
 * @license TODO
 */

use mod_vplcc\core\renderables\page;
use mod_vplcc\models\content;
use mod_vplcc\models\dimension;
use mod_vplcc\models\vplcc;

defined('MOODLE_INTERNAL') || die();

require_once($CFG->dirroot . '/course/moodleform_mod.php');
require_once($CFG->dirroot . '/mod/vplcc/lib.php');

/**
 * Class mod_vplcc_mod_form
 *
 * VPL CC plugin crud form definition
 */
class mod_vplcc_mod_form extends moodleform_mod {

    /**
     * Populates the crud form
     *
     * @param mixed $default
     */
    function set_data($default) {

        if (is_object($default)) {
            $default = (array) $default;
        }

        if (isset($default['update'])) {

            $vplcc = vplcc::find_or_fail($default['instance']);

            $content = content::find_or_fail([
                'vplccid' => $vplcc->id,
            ]);

            $default['instructions'] = [
                'text' => $content->instructions,
                'format' => $content->instructionsformat,
            ];

            $default['specifications'] = [
                'text' => $content->specifications,
                'format' => $content->specificationsformat,
            ];

            // dimensions are populated in the form definition

            $default['release'] = $vplcc->starttimestamp;

            foreach ($this->_form->getElement('vplcm')->_options as $key => $option) {
                if ($default['vplcm'] == $option['attr']['value']) {
                    unset($this->_form->getElement('vplcm')->_options[$key]['attr']['disabled']);
                }
            }

        }

        parent::set_data($default);
    }

    /**
     * Defines the vplcc crud form fields
     */
    protected function definition() {
        global $DB, $PAGE;

        $mform = &$this->_form;

        $mform->addElement('header', 'general', get_string('general', 'form'));

        // Name field.
        $mform->addElement('text', 'name', get_string('name'), ['size' => '255']);
        $mform->setType('name', PARAM_TEXT);
        $mform->addRule('name', null, 'required', null, 'client');
        $mform->applyFilter('name', 'trim');

        // Description field.
        $this->standard_intro_elements(get_string('description'));

        $mform->addElement('header', 'content', get_string('mod_form:content', 'vplcc'));

        // Instructions
        $mform->addElement('editor', 'instructions', get_string('mod_form:instructions', 'vplcc'), [
            'optional' => true
        ]);
        $mform->setType('instructions', PARAM_RAW);

        // Specifications
        $mform->addElement('editor', 'specifications', get_string('mod_form:specifications', 'vplcc'), [
            'optional' => true
        ]);
        $mform->setType('specifications', PARAM_RAW);

        $mform->addElement('header', 'vplcm_section', get_string('mod_form:section:vplcm', 'vplcc'));
        $mform->setExpanded('vplcm_section');

        $vplcms = get_coursemodules_in_course('vpl', $this->current->course);

        if (empty($vplcms)) {
            $mform->addElement('static', 'vplcm',
                get_string('mod_form:vplcm:activity', 'vplcc'),
                get_string('mod_form:vplcm:activity:empty', 'vplcc'));
        } else {

            $used = array_reduce($DB->get_records_list('vplcc', 'vplcm', array_keys($vplcms), ''), function($carry, $vplcc) {
                $carry[] = $vplcc->vplcm;
                return $carry;
            }, []);

            $select = $mform->createElement('select', 'vplcm', get_string('mod_form:vplcm:activity', 'vplcc'));
            $select->addOption(get_string('mod_form:vplcm:activity:select', 'vplcc'), '', ['disabled' => 'disabled', 'selected' => 'selected']);

            foreach ($vplcms as $id => $vplcm) {
                $string = strip_tags(format_string($vplcm->name));
                $options = [];

                if (in_array($id, $used)) {
                    $string .= ' ' . get_string('mod_form:vplcm:activity:used_affix', 'vplcc');
                    $options['disabled'] = 'disabled';
                }

                $select->addOption($string, $id, $options);
            }

            $mform->addElement($select);

            $mform->addRule('vplcm', get_string('error:mod_form:missingvpl', 'vplcc'), 'numeric', null, 'client');
        }

        $mform->addElement('header', 'leaderboard', get_string('mod_form:section:leaderboard', 'vplcc'));

        $mform->addElement('date_time_selector', 'release', get_string('mod_form:leaderboard:release', 'vplcc'), [
            'optional' => true
        ]);

        $groupings = groups_get_all_groupings($this->current->course);
        $mform->addElement('selectgroups', 'scope', get_string('mod_form:leaderboard:scope', 'vplcc'), [
            '0' => [
                vplcc::SCOPE_NONE => get_string('mod_form:leaderboard:scope:none', 'vplcc'),
                vplcc::SCOPE_ATTACHED_GROUPING => get_string('mod_form:leaderboard:scope:attached', 'vplcc')
            ],
            get_string('mod_form:leaderboard:scope:groupings', 'vplcc') => array_reduce($groupings, function ($carry, $grouping) {
                $carry[$grouping->id] = $grouping->name;
                return $carry;
            }, [])
        ]);
        $mform->setType('scope', PARAM_INT);

        $dimensionfieldsgroup = [
            $mform->createElement('text', 'slug', get_string('mod_form:leaderboard:dim:slug', 'vplcc'), [
                'placeholder' => get_string('mod_form:leaderboard:dim:slug', 'vplcc'),
                'class' => 'text-monospace d-block',
                'style' => 'font-size: 90%; height: calc(1.65em + .75rem + 2px);',
            ]),
            $mform->createElement('text', 'name', get_string('mod_form:leaderboard:dim:name', 'vplcc'), [
                'placeholder' => get_string('mod_form:leaderboard:dim:name', 'vplcc'),
                'class' => 'd-block',
            ]),
            $mform->createElement('text', 'description', get_string('mod_form:leaderboard:dim:desc', 'vplcc'), [
                'placeholder' => get_string('mod_form:leaderboard:dim:desc', 'vplcc'),
                'class' => 'd-block',
            ]),
        ];

        $dimensionfields = [
            $mform->createElement('group', 'dimension', get_string('mod_form:leaderboard:dim:label', 'vplcc'), $dimensionfieldsgroup),
            $mform->createElement('checkbox', 'delete_dimension', get_string('mod_form:leaderboard:dim:delete', 'vplcc')),
        ];

        $dimensionfieldsoptions = [
            'dimension[slug]' => [
                'type' => PARAM_ALPHANUMEXT,
            ],
            'dimension[name]' => [
                'type' => PARAM_TEXT,
            ],
            'dimension[description]' => [
                'type' => PARAM_TEXT,
            ]
        ];

        $mform->addElement('html', $PAGE->get_renderer('mod_vplcc')->render(new page('mod_vplcc/forms/dimensions_header', [])));

        $repeatno = 0;
        if ($this->_instance) {
            $dimensions = dimension::all([
                'vplccid' => $this->_instance,
            ]);
            if ($dimensions !== false) {
                $repeatno = count($dimensions);
                $default = [];
                foreach (array_values($dimensions) as $index => $dimension) {
                    $default[] = [
                        'slug' => $dimension->slug,
                        'name' => $dimension->name,
                        'description' => $dimension->description,
                    ];
                }
                $mform->setDefault('dimension', $default);
            }
        }

        $this->repeat_elements(
            $dimensionfields,
            $repeatno,
            $dimensionfieldsoptions,
            'dimensions',
            'dimensions_add_fields',
            1,
            get_string('mod_form:leaderboard:dim:add', 'vplcc'),
            true
        );

        $PAGE->requires->js_call_amd('mod_vplcc/mod_form', 'init');
        $PAGE->requires->css('/mod/vplcc/css/selectize.bootstrap4.css');

        $selectcols = $mform->addElement('select', 'columns_visual', get_string('mod_form:leaderboard:columns', 'vplcc'), [], [
            'data-doggle' => 'selectize',
            'placeholder' => get_string('mod_form:leaderboard:columns:placeholder', 'vplcc'),
        ]);
        $selectcols->setMultiple(true);

        $selectmeta = $mform->addElement('select', 'metadata_visual', get_string('mod_form:leaderboard:meta', 'vplcc'), [], [
            'data-doggle' => 'selectize',
            'placeholder' => get_string('mod_form:leaderboard:meta:placeholder', 'vplcc'),
        ]);
        $selectmeta->setMultiple(true);

        $mform->addElement('hidden', 'columns');
        $mform->setType('columns', PARAM_RAW);
        $mform->addElement('hidden', 'metadata');
        $mform->setType('metadata', PARAM_RAW);
        $mform->setDefault('columns', join(',', ['identifier', 'grade']));

        $this->standard_coursemodule_elements();

        $this->add_action_buttons();

    }

    /**
     * Validates the crud form
     *
     * @param array $data
     * @param array $files
     * @return array
     */
    public function validation($data, $files) {
        $errors = [];

        // VPL course module checks
        if (isset($data['vplcm']) && !empty($data['vplcm'])) {
            if (! get_coursemodule_from_id('vpl', $data['vplcm'], $this->current->course)) {
                $errors['vplcm'] = get_string('error:mod_form:notfoundvpl', 'vplcc');
            } else {
                $selected = vplcc::find(['vplcm' => $data['vplcm']]);
                if ((isset($data['instance']) && $selected !== false && $selected->id != $data['instance'])) {
                    $errors['vplcm'] = get_string('error:mod_form:usedvpl', 'vplcc');
                }
            }
        } else {
            $errors['vplcm'] = get_string('error:mod_form:missingvpl', 'vplcc');
        }

        $dimensions = isset($data['dimension']) ? $data['dimension'] : [];

        // Filter out dimensions for deletion
        if (isset($data['delete_dimension']) && count($data['delete_dimension']) > 0) {
            $dimensions = array_diff_key($data['dimension'], $data['delete_dimension']);
        }

        // Custom dimensions checks
        if ($data['dimensions'] > 0) {

            $slugcount = array_count_values(array_column($dimensions, 'slug'));

            foreach ($dimensions as $key => $dimension) {
                if (! isset($data['delete_dimension'][$key]) || ! $data['delete_dimension'][$key] == 1) {
                    if (! isset($dimension['slug']) || empty($dimension['slug'])) {
                        $errors['dimension[' . $key . ']'] = get_string('error:mod_form:dim:slug:missing', 'vplcc');

                    } else if (! preg_match('/^[a-zA-Z]+[a-zA-Z0-9._]+$/', $dimension['slug'])) {
                        $errors['dimension[' . $key . ']'] = get_string('error:mod_form:dim:slug:notalpha', 'vplcc');

                    } else if (strlen($dimension['slug']) > 50) {
                        $errors['dimension[' . $key . ']'] = get_string('error:mod_form:dim:slug:toolong', 'vplcc');

                    } else if ($dimension['slug'] == 'identifier' || $dimension['slug'] == 'grade') {
                        $errors['dimension[' . $key . ']'] = get_string('error:mod_form:dim:slug:reserved', 'vplcc', format_string($dimension['slug']));

                    } else if ($slugcount[$dimension['slug']] > 1) {
                        $errors['dimension[' . $key . ']'] = get_string('error:mod_form:dim:slug:unique', 'vplcc');

                    } else if (strlen($dimension['name']) > 100) {
                        $errors['dimension[' . $key . ']'] = get_string('error:mod_form:dim:name:toolong', 'vplcc');
                    }
                }
            }
        }

        $dimensionsslugs = array_merge(array_column($dimensions, 'slug'), ['identifier', 'grade']);

        // Columns validation
        if (isset($data['columns']) && !empty($data['columns'])) {

            $columns = explode(',', $data['columns']);

            if (count($columns) > 0) {
                if (! in_array('identifier', $columns)) {
                    $errors['columns_visual'] = get_string('error:mod_form:col:missingid', 'vplcc');
                } else {
                    foreach ($columns as $column) {
                        if (! preg_match('/^[a-zA-Z]+[a-zA-Z0-9._]+$/', $column)) {
                            $errors['columns_visual'] = get_string('error:mod_form:col:notalpha', 'vplcc', format_string($column));
                        } else if (! in_array($column, $dimensionsslugs)) {
                            $errors['columns_visual'] = get_string('error:mod_form:col:undefined', 'vplcc', format_string($column));
                        }
                    }
                }
            } else {
                $errors['columns_visual'] = get_string('error:mod_form:col:nocols', 'vplcc');
            }

        } else {
            $errors['columns_visual'] = get_string('error:mod_form:required', 'vplcc');
        }

        // Metadata valudation
        if (isset($data['metadata']) && !empty($data['metadata'])) {
            $metadata = explode(',', $data['metadata']);
            if (count($metadata) > 0) {
                foreach ($metadata as $meta) {
                    if (! preg_match('/^[a-zA-Z]+[a-zA-Z0-9._]+$/', $meta)) {
                        $errors['metadata_visual'] = get_string('error:mod_form:col:notalpha', 'vplcc', format_string($meta));
                    } else if (! in_array($meta, $dimensionsslugs)) {
                        $errors['metadata_visual'] = get_string('error:mod_form:col:undefined', 'vplcc', format_string($meta));
                    }
                }
            }
        }

        return $errors;
    }
}