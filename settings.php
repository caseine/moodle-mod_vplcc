<?php
// This file is part of VPL Code Challenges for Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * @author Photis Avrilionis
 * @copyright 2020 Photis Avrilionis
 *
 * This software was developed with the support of the following organizations:
 * - Université Grenoble Alpes
 * - Institut Polytechnique de Grenoble
 */

defined('MOODLE_INTERNAL') || die();

$settings->add(
    new admin_setting_configtext(
        'mod_vplcc/records_per_page',
        get_string('settings:recordspp', 'vplcc'),
        get_string('settings:recordspp:desc', 'vplcc'),
        30,
        PARAM_INT
    )
);

$settings->add(
    new admin_setting_configcheckbox(
        'mod_vplcc/show_feedback',
        get_string('settings:feedback', 'vplcc'),
        get_string('settings:feedback:desc', 'vplcc'),
        1
    )
);
