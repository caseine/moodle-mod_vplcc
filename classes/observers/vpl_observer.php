<?php
// This file is part of VPL Code Challenges for Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * @author Photis Avrilionis
 * @copyright 2020 Photis Avrilionis
 *
 * This software was developed with the support of the following organizations:
 * - Université Grenoble Alpes
 * - Institut Polytechnique de Grenoble
 */

namespace mod_vplcc\observers;

defined('MOODLE_INTERNAL') || die();

use Exception;
use mod_vpl\event\submission_evaluated;
use mod_vpl_submission;
use mod_vplcc\models\evaluation;
use mod_vplcc\models\vplcc;

class vpl_observer {

    /**
     * Listen to VPL evaluations and records the details of the most recent evaluation
     * when they are attached to a VPL Code Challenge activity.
     *
     * Simplifying assumption: a single code challenge can be attached to a VPL activity. This is not
     *    enforced in the database definition as this is a feature planned for future versions of the plugin.
     *
     * @param submission_evaluated $event
     */
    public static function register_evaluation(submission_evaluated $event) {
        global $DB;

        try {

            $vplcc = vplcc::find([
                'vplcm' => $event->get_context()->instanceid,
            ]);

            if ($vplcc !== false) {

                $transaction = $DB->start_delegated_transaction();

                try {

                    $vpl = new \mod_vpl($vplcc->vplcm);
                    $submission = new mod_vpl_submission($vpl, $event->objectid);

                    $criteria = [
                        'vplccid' => $vplcc->id,
                    ];

                    if ($vpl->is_group_activity()) {
                        $criteria['groupid'] = $submission->get_instance()->groupid;
                        $criteria['userid'] = 0;
                    } else {
                        $criteria['userid'] = $submission->get_instance()->userid;
                        $criteria['groupid'] = 0;
                    }

                    $evaluation = evaluation::find($criteria);

                    $excludedfromleaderboard = ($vpl->is_group_activity() === false)
                        && ($evaluation === false)
                        && (
                            has_capability(VPL_GRADE_CAPABILITY, $event->get_context(), $submission->get_instance()->userid)
                            || has_capability(VPL_MANAGE_CAPABILITY, $event->get_context(), $submission->get_instance()->userid)
                        );

                    if (! $excludedfromleaderboard) {

                        if ($evaluation === false) {
                            $evaluation = new evaluation($criteria);
                        }

                        $evaluation->vplsubmissionid = $submission->get_instance()->id;
                        $evaluation->lastupdated = 0;

                        $evaluation->save();

                    }

                    $transaction->allow_commit();

                } catch (Exception $e) {

                    $transaction->rollback($e);
                }
            }

        } catch (Exception $e) {

            /*
             * eats and logs all errors
             * Read Event Dispatching at https://docs.moodle.org/dev/Events_API#Event_dispatching
             */

             error_log("[mod_vplcc observer] " . $e->getMessage());
             
        }
    }
}
