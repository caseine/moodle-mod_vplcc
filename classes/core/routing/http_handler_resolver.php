<?php
// This file is part of VPL Code Challenges for Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * @author Photis Avrilionis
 * @copyright 2020 Photis Avrilionis
 *
 * This software was developed with the support of the following organizations:
 * - Université Grenoble Alpes
 * - Institut Polytechnique de Grenoble
 */

namespace mod_vplcc\core\routing;

defined('MOODLE_INTERNAL') || die();

use mod_vplcc\core\contracts\bus\handler_resolver_contract;
use mod_vplcc\core\http\request;

class http_handler_resolver implements handler_resolver_contract {

    /**
     * The application's router instance
     * @var router
     */
    protected $router;

    public function __construct(router $router) {
        $this->router = $router;
    }

    /**
     * Resolves a request into a controller
     * @param request $request
     * @return callback
     */
    public function resolve($request) {
        $route = $this->find_route($request);

        if (isset($route) === false) {
            print_error('notfound', 'vplcc');
        }

        if ($route['method'] !== $request->get_method()) {
            print_error('methodnotallowed', 'vplcc');
        }

        $action = explode('+', $route['uses']['action']);

        $namespace = $route['uses']['namespace'];
        $controller = $action[0];
        $method = $action[1];

        $controller = $namespace . '\\' . $controller;

        // Throw error if not found, or method not allowed.

        // Dependency injection for controller the would be done here.

        return [new $controller, $method];
    }

    /**
     * Finds the route from a request instance
     * @param request $request
     */
    private function find_route(request $request) {
        $path = $request->get_path();
        return $this->router->routes()[$path];
    }

}