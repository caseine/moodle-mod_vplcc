<?php
// This file is part of VPL Code Challenges for Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * @author Photis Avrilionis
 * @copyright 2020 Photis Avrilionis
 *
 * This software was developed with the support of the following organizations:
 * - Université Grenoble Alpes
 * - Institut Polytechnique de Grenoble
 */

namespace mod_vplcc\core;

defined('MOODLE_INTERNAL') || die();

use coding_exception;

/**
 * Class singleton.
 *
 * Singletons ensure that only one instance of an object exists throughout
 * the application lifecycle.
 *
 * This singleton class is implemented based on refactoring.guru's example.
 *
 * @link https://refactoring.guru/design-patterns/singleton/php/example
 * @package mod_vplcc\core
 */
class singleton {

    private static $instances = [];

    protected function __construct() {
    }

    /**
     * Prevent singleton cloning.
     */
    protected function __clone() {
        throw new coding_exception("Cannot clone a singleton.");
    }

    /**
     * Prevent singleton unserialization.
     */
    public function __wakeup() {
        throw new coding_exception("Cannot unserialize a singleton.");
    }

    public static function get_instance() {
        $cls = static::class;

        if (!isset(self::$instances[$cls])) {
            self::$instances[$cls] = new static;
        }

        return self::$instances[$cls];
    }

}
