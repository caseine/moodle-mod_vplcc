<?php
// This file is part of VPL Code Challenges for Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * @author Photis Avrilionis
 * @copyright 2020 Photis Avrilionis
 *
 * This software was developed with the support of the following organizations:
 * - Université Grenoble Alpes
 * - Institut Polytechnique de Grenoble
 */

namespace mod_vplcc\core;

defined('MOODLE_INTERNAL') || die();

use mod_vplcc\core\http\request;
use mod_vplcc\core\http\response;
use mod_vplcc\core\routing\http_dispatcher;
use mod_vplcc\core\routing\http_handler_resolver;
use mod_vplcc\core\routing\router;

class app {

    /**
     * The request instance
     * @var request;
     */
    public $request;

    /**
     * The router instance
     *
     * @var router
     */
    public $router;

    /**
     * Creates a vplcc application instance
     */
    public function __construct() {
        $this->prepare_request();
        $this->prepare_router();
    }

    /**
     * Creates the request instance for the vplcc application
     */
    protected function prepare_request() {
        $this->request = request::get_instance();
    }

    /**
     * Creates the router instance for the vplcc application
     */
    protected function prepare_router() {
        $this->router = new router($this);
    }

    /**
     * Runs the application instance
     */
    public function run() {

        $dispatcher = new http_dispatcher(new http_handler_resolver($this->router));

        $response = $dispatcher->dispatch($this->request);

        if ($response instanceof response) {
            $response->send();

        } else {
            echo (string) $response;
        }
    }
}
