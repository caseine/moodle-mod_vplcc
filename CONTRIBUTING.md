# Contributing

## Pull request process

// TODO

## Development installation using Docker Compose

We suggest you use the `docker-compose.yml` recipe of this project to run the plugin on you machine. This Docker Compose recipe will create the services needed to run Moodle 3.9 and will sync the plugin files with the installation. The recipe uses Bitnami's [MySQL](https://github.com/bitnami/bitnami-docker-moodle) and [Moodle](https://github.com/bitnami/bitnami-docker-moodle) Docker images.

The installation will take you about 5 minutes of hands-on time, and about 15-20 minutes to download and install Moodle.

Follow the steps below to get a Moodle 3.9 installation up and running _(please bear with me on this, I don't have much experience with Docker yet - input for improvements will be highly appreciated!)_:

1. Get the [latest version of Docker](https://www.docker.com/get-started).
2. `cd` to this directory and run the command below. The `-d` flag (detach) will run the containers in the background. The Docker containers may take some time to start up when you run this for the first time.

   ```
   $ docker compose up -d
   ```

3. When Docker is done working, you should already be able to access Moodle at <http://localhost/>. The credentials of the admin user will be `user` and `bitnami`.
4. Download the VPL plugin (for Caseine developers, use https://gricad-gitlab.univ-grenoble-alpes.fr/caseine/moodle-mod_vpl/) and install it in the Moodle site (go to Site administration > Plugins > Install plugin).
5. SSH into the Docker container (see how to [here](https://phoenixnap.com/kb/how-to-ssh-into-docker-container)) and run the command below. This command will replicate the plugin's directory structure under the container's Moodle installation. Symbolic links wouldn't work, as resolving relative paths in PHP would be scoped to the symlink's source location. The `chwon` command will also ensure the container's worker will have access to the plugin's files.

   ```
   $ mount --bind /bitnami/plugin /bitnami/moodle/mod/vplcc
   $ chown -R daemon:root /bitnami/moodle/mod/vplcc
   ```

   Once completed, you can close the ssh connection with the container and visit the local Moodle site again. Moodle will detect the new directory and ask you to install the code challenges plugin, just follow the instructions on the screen.

   **Note:** the `/bitnami/plugin` directory in the container is synchronised with the directory containing the cloned project on the host machine. Changes to files on the host machine will be reflected in the Docker container and vice versa.

6. Enjoy your fresh Moodle setup 🍹

## Additional configuration

### PhpStorm Coding Hints

You can enable PhpStorm Coding Hints to get autocompletion, and other code hints by adding Moodle as an external library to your project. After importing the plugin into a new PhpStorm project, follow the instructions below:

1. Clone version 3.9 of Moodle to a directory on your machine. To avoid cloning the entire repository, you can use the following command:

   ```
   $ git clone --depth 1 --branch v3.9.2 https://github.com/moodle/moodle.git /path/to/dir/
   ```

2. Open the "Projects" pane and right click on "External Libraries". Select "Configure PHP Include Paths ...".
3. In the window that opens, under the "Include Path" tab, click on the + button and choose the directory where you have cloned Moodle.
4. Apply the changes and wait for PhpStorm to index Moodle.
