<?php
// This file is part of VPL Code Challenges for Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * @author Photis Avrilionis
 * @copyright 2020 Photis Avrilionis
 *
 * This software was developed with the support of the following organizations:
 * - Université Grenoble Alpes
 * - Institut Polytechnique de Grenoble
 */

namespace mod_vplcc\core\http;

defined('MOODLE_INTERNAL') || die();

use mod_vplcc\core\singleton;

class request extends singleton {

    const METHOD_GET = 'GET';
    const METHOD_POST = 'POST';

    /**
     * The request method
     * @var string
     */
    protected $method = null;

    /**
     * The equivalent of the request uri for the Moodle plugin
     *
     * @var string
     */
    protected $path = null;

    /**
     * The request entry point
     *
     * @var string
     */
    protected $entrypoint = null;

    /**
     * The request parameters
     */
    protected $params = null;

    /**
     * Parses the page parameter to determine controller and method to be called.
     *
     * @return array
     */
    public function get_path() {
        if ($this->path !== null) {
            return $this->path;
        }

        return $this->path = filter_var(rtrim($this->param('p', PARAM_PATH), '.'), FILTER_SANITIZE_URL);
    }

    /**
     * Detects and returns the request method.
     *
     * @return string
     */
    public function get_method() {
        if ($this->method !== null) {
            return $this->method;
        }

        return $this->method = strtoupper($_SERVER['REQUEST_METHOD']);
    }

    /**
     * Detects and returns the request entry point.
     *
     * @return string
     */
    public function get_entry_point() {
        if ($this->entrypoint !== null) {
            return $this->entrypoint;
        }

        return $this->entrypoint = explode('?', $_SERVER['REQUEST_URI'], 2)[0];
    }

    /**
     * Returns an array of all parameters passed to the request
     *
     * @return array
     */
    public function params() {
        if ($this->params !== null) {
            return $this->params;
        }

        $this->params = $_GET + $_POST;
        unset($this->params['p']);

        return $this->params;
    }

    /**
     * Retrieves a parameter using Moodle's core library functions
     *
     * @param string $name
     * @param string $type the Moodle parameter constants
     * @param string $default the default
     * @param boolean $required
     * @return string
     */
    public function param($name, $type, $default = null, $required = false) {
        if ($required) {
            return required_param($name, $type);
        }

        return optional_param($name, $default, $type);
    }
}
