<?php
// This file is part of VPL Code Challenges for Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * @author Photis Avrilionis
 * @copyright 2020 Photis Avrilionis
 *
 * This software was developed with the support of the following organizations:
 * - Université Grenoble Alpes
 * - Institut Polytechnique de Grenoble
 */

namespace mod_vplcc\models;

defined('MOODLE_INTERNAL') || die();

use mod_vplcc\core\database\model;

/**
 * Class content
 *
 * ORM model for vpl code challenge content instances.
 *
 * content contains the instructions and specifications text for
 *
 * @property int id the instance id
 * @property int vplccid the vplcc activity that the content belongs to
 * @property string instructions the instructions raw text
 * @property string instructionsformat the instructions text rendering format
 * @property string specifications the specifications raw text
 * @property string specificationsformat the specifications text rendering format
 *
 * @package mod_vplcc\models
 */
class content extends model {

    /**
     * @see model::$table
     */
    protected $table = "vplcc_content";

    /**
     * @see model::$attributes
     */
    protected $attributes = [
        'id',

        'vplccid',
        'instructions',
        'instructionsformat',
        'specifications',
        'specificationsformat',
    ];

    /**
     * @see model::$fillable
     */
    protected $fillable = [
        'vplccid',
        'instructions',
        'instructionsformat',
        'specifications',
        'specificationsformat',
    ];

}
