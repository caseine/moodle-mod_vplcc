<?php
// This file is part of VPL Code Challenges for Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * @author Photis Avrilionis
 * @copyright 2020 Photis Avrilionis
 *
 * This software was developed with the support of the following organizations:
 * - Université Grenoble Alpes
 * - Institut Polytechnique de Grenoble
 */

namespace mod_vplcc\resources;

defined('MOODLE_INTERNAL') || die();

use mod_vplcc\core\contracts\support\arrayable;
use mod_vplcc\core\http\request;
use mod_vplcc\core\routing\url;
use mod_vplcc\models\grid;

class pagination_resource implements arrayable {

    /**
     * @var grid
     */
    protected $pagination = null;

    /**
     * pagination_resource constructor.
     * @param $pagination
     */
    private function __construct($pagination) {
        $this->pagination = $pagination;
    }

    /**
     * Initialises the ressource from a $pagination array
     *
     * @param array $pagination pagination details array
     * @return static
     */
    public static function from($pagination) {
        return new static($pagination);
    }

    /**
     * Returns the url to the previous page if applicable
     *
     * @return bool|string
     */
    protected function previous() {
        if ($this->pagination['current'] > 1) {
            return url::route('leaderboard.view', array_merge(
                request::get_instance()->params(),
                ['page' => $this->pagination['current'] - 1]
            ))->out(false);
        }
        return false;
    }

    /**
     * Returns the url to the next page if applicable
     *
     * @return bool|string
     */
    protected function next() {
        if ($this->pagination['current'] < $this->pagination['total_pages']) {
            return url::route('leaderboard.view', array_merge(
                request::get_instance()->params(),
                ['page' => $this->pagination['current'] + 1]
            ))->out(false);
        }
        return false;
    }

    /**
     * Returns url and details for every page
     *
     * @return array
     */
    protected function pages() {
        $pages = [];
        $urlparams = array_merge(request::get_instance()->params(), ['page' => 0]);

        for ($page = 1; $page <= $this->pagination['total_pages']; $page++) {
            $urlparams['page'] = $page;
            $pages[] = [
                'current' => $page == $this->pagination['current'],
                'page' => $page,
                'link' => url::route('leaderboard.view', $urlparams)->out(false)
            ];
        }
        return $pages;
    }

    /**
     * @inheritDoc
     */
    public function to_array() {
        return [
            'enabled' => $this->pagination['total_pages'] > 1,
            'total_pages' => $this->pagination,
            'next' => $this->next(),
            'prev' => $this->previous(),
            'pages' => $this->pages()
        ];
    }
}