<?php
// This file is part of VPL Code Challenges for Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * @author Photis Avrilionis
 * @copyright 2020 Photis Avrilionis
 *
 * This software was developed with the support of the following organizations:
 * - Université Grenoble Alpes
 * - Institut Polytechnique de Grenoble
 */

namespace mod_vplcc\core\routing;

defined('MOODLE_INTERNAL') || die();

use moodle_url;
use mod_vplcc\core\http\request;

class url {

    /**
     * Create a Moodle url for a route name
     *
     * Note: future work would involve checking the route name against the defined
     * application routes. This would require url to access the router of the app instance.
     * This means that the url class would be dependent on an app instance.
     *
     * @param $name
     * @param null $params
     * @param null $anchor
     * @return moodle_url
     */
    public static function route($name, $params = [], $anchor = null) {
        $request = request::get_instance();

        $url = new moodle_url($request->get_entry_point());

        $url->param('p', $name);
        $url->params($params);
        $url->set_anchor($anchor);

        return $url;
    }

}