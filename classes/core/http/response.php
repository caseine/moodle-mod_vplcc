<?php
// This file is part of VPL Code Challenges for Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * @author Photis Avrilionis
 * @copyright 2020 Photis Avrilionis
 *
 * This software was developed with the support of the following organizations:
 * - Université Grenoble Alpes
 * - Institut Polytechnique de Grenoble
 */

namespace mod_vplcc\core\http;

defined('MOODLE_INTERNAL') || die();

use mod_vplcc\core\routing\url;
use renderable;
use renderer_base;

class response {

    /**
     * @var string
     */
    protected $content;

    /**
     * @var renderer_base
     */
    protected $output;

    /**
     * response constructor.
     *
     * @param string $content
     * @param string|renderer_base $renderer
     */
    public function __construct($content = "", $renderer = "") {
        $this->prepare_response();
        $this->set_renderer($renderer);
        $this->set_content($content);
    }

    public static function page() {
        global $PAGE;
        return $PAGE;
    }

    /**
     * Prepares the response to be sent
     */
    public function prepare_response() {
        if (! static::page()->has_set_url()) {
            static::page()->set_url(url::route('view', request::get_instance()->params()));
        }

        static::page()->set_include_region_main_settings_in_header_actions(true);
    }

    /**
     * Sets the renderer for the current response
     *
     * @param string|renderer_base $renderer
     */
    public function set_renderer($renderer) {
        global $OUTPUT;

        if (is_string($renderer) && !empty($renderer)) {
            $this->output = static::page()->get_renderer($renderer);

        } else if ($renderer instanceof renderer_base) {
            $this->output = $renderer;

        } else {
            $this->output = $OUTPUT;
        }
    }

    /**
     * Sets the content for the current response
     *
     * @param renderable|string $content
     */
    public function set_content($content = "") {

        if ($content instanceof renderable) {
            $this->content = $this->output->render($content);

        } else {
            $this->content = $content;
        }
    }

    /**
     * Sends content for the current response
     *
     * @return $this
     */
    public function send_content() {
        echo $this->content;
        return $this;
    }

    /**
     * Sends the Moodle header for the current response
     *
     * @return $this
     */
    public function send_header() {
        echo $this->output->header();
        return $this;
    }

    /**
     * Sends the Moodle footer for the current response
     *
     * @return $this
     */
    public function send_footer() {
        echo $this->output->footer();
        return $this;
    }

    /**
     * Sends the current response
     */
    public function send() {
        $this->send_header();
        $this->send_content();
        $this->send_footer();
    }
}
