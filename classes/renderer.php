<?php
// This file is part of VPL Code Challenges for Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * @author Photis Avrilionis
 * @copyright 2020 Photis Avrilionis
 *
 * This software was developed with the support of the following organizations:
 * - Université Grenoble Alpes
 * - Institut Polytechnique de Grenoble
 */

defined('MOODLE_INTERNAL') || die();

use mod_vplcc\core\renderables\page;

class mod_vplcc_renderer extends plugin_renderer_base {

    /**
     * Render via a template.
     *
     * @param page $page
     *
     * @return string HTML for the page
     * @throws moodle_exception
     */
    protected function render_page($page) {

        $template = $page->template();
        $data = $page->export_for_template($this);

        $mustache = $this->get_mustache();

        $mustache->addHelper('dump', function($value, Mustache_LambdaHelper $helper) {
            var_dump($helper->render($value));
            return null;
        });

        $mustache->addHelper('format', function($value, Mustache_LambdaHelper $helper) {
            list($text, $format) = explode(',', $value, 2);
            return format_text($helper->render($text),  $helper->render($format));
        });

        $mustache->addHelper('config', function($value, Mustache_LambdaHelper $helper) {
            list($plugin, $key) = explode(',', $value, 2);
            return get_config($plugin, $key);
        });

        return $this->render_from_template($template, $data);
    }
}
