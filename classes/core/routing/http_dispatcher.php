<?php
// This file is part of VPL Code Challenges for Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * @author Photis Avrilionis
 * @copyright 2020 Photis Avrilionis
 *
 * This software was developed with the support of the following organizations:
 * - Université Grenoble Alpes
 * - Institut Polytechnique de Grenoble
 */

namespace mod_vplcc\core\routing;

defined('MOODLE_INTERNAL') || die();

use mod_vplcc\core\bus\dispatcher;
use mod_vplcc\core\http\response;
use mod_vplcc\core\contracts\http\responsable;

class http_dispatcher extends dispatcher {

    /**
     * Calls the controller action that the route resolves to
     *
     * @param request $request
     * @return response|string
     */
    public function dispatch($request) {
        $handler = $this->resolver->resolve($request);

        // Future work: get handler parameters from Reflector, bind models, and pass them to the handler.

        return $this->prepare_response($handler());
    }

    /**
     * Prepares the response for sending.
     *
     * @param mixed $response
     * @return response|mixed
     */
    public function prepare_response($response) {

        if ($response instanceof responsable) {
            return $response->to_response();
        }

        return $response;
    }

}
