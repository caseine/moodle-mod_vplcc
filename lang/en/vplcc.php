<?php
// This file is part of VPL Code Challenges for Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * @author Photis Avrilionis
 * @copyright 2020 Photis Avrilionis
 *
 * This software was developed with the support of the following organizations:
 * - Université Grenoble Alpes
 * - Institut Polytechnique de Grenoble
 */

// Generic.
$string['vplcc:addinstance'] = 'Add a new VPL Code Challenge';
$string['vplcc:view'] = 'View VPL Code Challenge';
$string['modulename'] = 'VPL Code Challenge';
$string['modulename_help'] = '<p>The Virtual programming lab Code Challenge activity lets you create customisable code challenges on top of existing Virtual programming labs.</p><p><a target="_blank" href="https://pavril.github.io/vpl-code-challenges-docs/#/advanced/configuration-kitchen-sink">Get help for setting up code challenges</a></p><p><a  target="_blank" href="https://pavril.github.io/vpl-code-challenges-docs/">Plugin documentation</a></p>';
$string['modulenameplural'] = 'VPL Code Challenges';
$string['privacy:metadata'] = 'The VPL Code Challenge activity plugin only stores user ids or group ids of every user that triggers an evaluation on a VPL activity that\'s attached to a code challenge.';
$string['pluginadministration'] = 'VPL Code Challenge administration';
$string['pluginname'] = 'Virtual programming lab Code Challenge';
$string['search:activity'] = 'VPL Code Challenge';
$string['menu:editattached'] = 'Edit settings of attached activity';

// Module form strings.
$string['mod_form:content'] = "Content";
$string['mod_form:instructions'] = "Instructions";
$string['mod_form:specifications'] = "Specifications";
$string['mod_form:availability'] = "Availability";
$string['mod_form:leaderboard'] = "Leaderboard";
$string['mod_form:section:vplcm'] = "Virtual Programming Lab activity configuration";
$string['mod_form:vplcm:activity'] = "Attached VPL Activity";
$string['mod_form:vplcm:activity:empty'] = "There are no VPL activities in this course.";
$string['mod_form:vplcm:activity:select'] = "Select a VPL activity ...";
$string['mod_form:vplcm:activity:used_affix'] = "(used)";
$string['mod_form:section:leaderboard'] = "Leaderboard configuration";
$string['mod_form:leaderboard:release'] = "Announcement date";
$string['mod_form:leaderboard:scope'] = "Scope restrictions";
$string['mod_form:leaderboard:scope:none'] = "None";
$string['mod_form:leaderboard:scope:attached'] = "Use attached activity grouping";
$string['mod_form:leaderboard:scope:groupings'] = "Groupings";
$string['mod_form:leaderboard:header:title'] = "Leaderboard dimensions";
$string['mod_form:leaderboard:header:tag'] = "Advanced";
$string['mod_form:leaderboard:header:lead'] = "Declare the dimensions generated during VPL evaluations in order to add them to the leaderboard.";
$string['mod_form:leaderboard:header:docs'] = "Read more on dimensions";
$string['mod_form:leaderboard:header:dim:id'] = "Identifier dimension";
$string['mod_form:leaderboard:header:dim:id:name'] = "Name/Group";
$string['mod_form:leaderboard:header:dim:id:desc'] = "No description";
$string['mod_form:leaderboard:header:dim:grade'] = "Grade dimension";
$string['mod_form:leaderboard:header:dim:grade:name'] = "Grade";
$string['mod_form:leaderboard:dim:slug'] = "Slug";
$string['mod_form:leaderboard:dim:name'] = "Name";
$string['mod_form:leaderboard:dim:desc'] = "Description";
$string['mod_form:leaderboard:dim:label'] = "Custom dimension {no}";
$string['mod_form:leaderboard:dim:delete'] = "Delete custom dimension {no} on save";
$string['mod_form:leaderboard:dim:add'] = "Add new dimension";
$string['mod_form:leaderboard:columns'] = "Column dimensions";
$string['mod_form:leaderboard:columns:placeholder'] = "Define the dimensions to be shown as a column";
$string['mod_form:leaderboard:meta'] = "Metadata dimensions";
$string['mod_form:leaderboard:meta:placeholder'] = "Define the dimensions to be shown as a metadata";

// Errors.
$string['error:mod_form:required'] = "This field is required.";
$string['error:mod_form:missingvpl'] = "You must choose a VPL activity";
$string['error:mod_form:notfoundvpl'] = "The selected VPL activity does not exist.";
$string['error:mod_form:usedvpl'] = "The selected VPL activity is used by another code challenge.";
$string['error:mod_form:dim:slug:missing'] = "The slug is required.";
$string['error:mod_form:dim:slug:notalpha'] = "The slug must be an alphanumeric string.";
$string['error:mod_form:dim:slug:toolong'] = "The slug must be less than 50 characters.";
$string['error:mod_form:dim:slug:reserved'] = 'The <span class="text-monospace" style="font-size: 95%;">{$a}</span> slug is reserved.';
$string['error:mod_form:dim:slug:unique'] = 'The slugs must be unique, rename or remove duplicate slug.';
$string['error:mod_form:dim:name:toolong'] = 'The name must be less than 100 characters.';
$string['error:mod_form:col:missingid'] = 'The <span class="text-monospace" style="font-size: 90%;">identifier</span> dimension must always be included.';
$string['error:mod_form:col:notalpha'] = 'The <span class="text-monospace" style="font-size: 90%;">{$a}</span> slug is not an alphanumeric string.';
$string['error:mod_form:col:undefined'] = 'The <span class="text-monospace" style="font-size: 90%;">{$a}</span> dimension is not defined.';
$string['error:mod_form:col:nocols'] = 'At least one column must be defined.';

// Navigation.
$string['navigation:overview'] = "Overview";
$string['navigation:leaderboard'] = "Leaderboard";
$string['navigation:lastupdate'] = "Last updated: ";

// Summary.
$string['summary:call_to_action'] = "Open activity";
$string['summary:partof'] = "Part of";
$string['summary:individual'] = "Individual submissions";
$string['summary:grouped'] = "Grouped submissions";
$string['summary:memberof'] = "You are a member of";
$string['summary:nogroup'] = "You do not belong to any group on this activity";
$string['summary:start'] = "Start date";
$string['summary:due'] = "Due date";

// Index page.
$string['index:title'] = "VPL Code Challenge instances";
$string['index:heading'] = "VPL Code Challenge instances";
$string['index:table:id'] = "Id";
$string['index:table:name'] = "Name";
$string['index:table:created'] = "Created";
$string['index:table:view'] = "View activity";
$string['index:table:empty'] = "There are no code challenge instances in this course";

// Leaderboard partials.
// Countdown.
$string['leaderboard:countdown:title'] = "You're still in competition!";
$string['leaderboard:countdown:lead'] = "You will be able to see the leaderboard once the challenge is over.";
$string['leaderboard:countdown:availablefrom'] = "The leaderboard will be available from ";

// Empty.
$string['leaderboard:empty:title'] = "Check back later!";
$string['leaderboard:empty:lead'] = "There are no submissions to show at this time, submit your work and check back later.";
$string['leaderboard:nodata'] = "No data";

// List.
$string['leaderboard:list:alternatename'] = "If provided, your alternate name will be used instead of your full name. Set it in the <b>Additional names</b> section of your profile settings.";
$string['leaderboard:list:public'] = "Public";
$string['leaderboard:list:details:view'] = "View details";
$string['leaderboard:list:details:title'] = "Evaluation details";
$string['leaderboard:list:group'] = "Group";
$string['leaderboard:list:name'] = "Name";
$string['leaderboard:list:grade'] = "Grade";
$string['leaderboard:noncompeting'] = "You do not belong to any competition group to access this leaderboard.";

// Scope selection.
$string['leaderboard:scopeswitch'] = "Switch scope ...";

// Feedback form.
$string['messages.feedback.call_to_action'] = "Help &amp; Feedback";
$string['messages.feedback.subtitle'] = "Development feedback form";
$string['messages.feedback.lead'] = "You are using an early version of the VPL Code Challenges plugin. Some features are still under development, while others require user feedback to refine them.";
$string['messages.feedback.indicator.version'] = "Installed plugin build";
$string['messages.feedback.indicator.external_tool'] = "External tool";
$string['messages.feedback.indicator.restricted'] = "Restricted access";
$string['messages.feedback.form.title'] = "Give feedback";
$string['messages.feedback.form.description'] = "Your feedback helps us to better understand how you use the plugin, so we can focus on developing the features that are the most important to users.";
$string['messages.feedback.mattermost.title'] = "Mattermost discussion";
$string['messages.feedback.mattermost.description'] = "Get in touch directly with the plugin developers: give your feedback, ask questions, discuss and suggest new feature ideas, or make any other requests.";
$string['messages.feedback.issues.title'] = "Report issues";
$string['messages.feedback.issues.description'] = "Should you encounter bugs or other technical or functional issues while using the plugin, please report them on the plugin development page.";
$string['messages.feedback.documentation.title'] = "Read documentation";
$string['messages.feedback.documentation.description'] = "Find practical guides, tutorials and other technical details on how to use the plugin.";

// Reset form.
$string['messages:reset:all_entries'] = "Delete all existing leaderboard entries";
$string['messages:reset:flushed_vplcc'] = 'Delete leaderboard entries for {$a}';
$string['messages:reset:could_not_delete'] = 'Could not delete entries';

// Settings.
$string['settings:recordspp'] = "Leaderboard records per page";
$string['settings:recordspp:desc'] = "The maximum entries to display in a leaderboard page.";
$string['settings:feedback'] = "Show feedback link";
$string['settings:feedback:desc'] = "Display feedback and bug filing link on every code challenge page.";
