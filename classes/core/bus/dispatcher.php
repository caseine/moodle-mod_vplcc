<?php
// This file is part of VPL Code Challenges for Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * @author Photis Avrilionis
 * @copyright 2020 Photis Avrilionis
 *
 * This software was developed with the support of the following organizations:
 * - Université Grenoble Alpes
 * - Institut Polytechnique de Grenoble
 */

namespace mod_vplcc\core\bus;

defined('MOODLE_INTERNAL') || die();

use mod_vplcc\core\contracts\bus\dispatcher_contract;
use mod_vplcc\core\contracts\bus\handler_resolver_contract;

class dispatcher implements dispatcher_contract {

    /**
     * The dispatcher's handler resolver.
     * @var mixed
     */
    protected $resolver;

    /**
     * Creates a new instance of a dispatcher.
     * @param handler_resolver_contract $resolver the handler resolver for the dispatcher
     */
    public function __construct(handler_resolver_contract $resolver) {
        $this->resolver = $resolver;
    }

    /**
     * Calls the resolved handler
     * {@inheritDoc}
     * @see dispatcher::dispatch()
     */
    public function dispatch($command) {
        $handler = $this->resolver->resolve($command);
        return $handler();
    }

}