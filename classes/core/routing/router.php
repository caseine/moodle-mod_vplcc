<?php
// This file is part of VPL Code Challenges for Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * @author Photis Avrilionis
 * @copyright 2020 Photis Avrilionis
 *
 * This software was developed with the support of the following organizations:
 * - Université Grenoble Alpes
 * - Institut Polytechnique de Grenoble
 */

namespace mod_vplcc\core\routing;

defined('MOODLE_INTERNAL') || die();

use mod_vplcc\core\app;

class router {

    /**
     * The vplcc application instance
     * @var app
     */
    public $app;

    /**
     * The collection of routes
     * @var array
     */
    protected $routes = [];

    /**
     * The default controller namespace
     * @var string
     */
    protected $namespace = "mod_vplcc\controllers";

    /**
     * Constructs the router instance
     * @param app $app
     */
    public function __construct(app $app) {
        $this->app = $app;
    }

    /**
     *
     * @param string $method
     * @param string $path
     * @param string $action
     * @param string $namespace
     */
    public function add_route($method, $path, $action, $namespace = null) {
        $this->routes[$path] = [
            'method' => $method,
            'uses' => [
                'namespace' => empty($namespace) ? $this->namespace : $namespace,
                'action' => $action
            ],
        ];
    }

    /**
     * Registers a get route
     * @param string $path
     * @param string $action
     * @param string $namespace
     */
    public function get($path, $action, $namespace = null) {
        $this->add_route("GET", $path, $action, $namespace);
    }

    /**
     * Registers a post route
     * @param string $path
     * @param string $action
     * @param string $namespace
     */
    public function post($path, $action, $namespace = null) {
        $this->add_route("POST", $path, $action, $namespace);
    }

    /**
     * Gets the routes of the application
     * @return array
     */
    public function routes() {
        return $this->routes;
    }
}
