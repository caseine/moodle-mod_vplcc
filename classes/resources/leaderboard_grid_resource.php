<?php
// This file is part of VPL Code Challenges for Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * @author Photis Avrilionis
 * @copyright 2020 Photis Avrilionis
 *
 * This software was developed with the support of the following organizations:
 * - Université Grenoble Alpes
 * - Institut Polytechnique de Grenoble
 */

namespace mod_vplcc\resources;

defined('MOODLE_INTERNAL') || die();

use mod_vplcc\core\contracts\support\arrayable;
use mod_vplcc\models\grid;

class leaderboard_grid_resource implements arrayable {

    /**
     * @var grid
     */
    protected $instance = null;

    /**
     * @var int the grid's index offset
     */
    protected $offset = 0;

    private function __construct(grid $instance, $offset = 0) {
        $this->instance = $instance;
        $this->offset = $offset;
    }

    public static function from(grid $instance, $offset = 0) {
        return new static($instance, $offset);
    }

    /**
     * Filters and orders the dictionary by keys and returns it as a associative array
     *
     * @param $dictionary
     * @param $keys
     * @return array
     */
    protected function filter_keys($dictionary, $keys) {
        return array_reduce($keys, function($carry, $key) use ($dictionary) {
            $carry[$key] = $dictionary[$key];
            return $carry;
        }, []);
    }

    /**
     * Filters and orders the dictionary by keys and returns it as a sequential array
     *
     * @param $dictionary
     * @param $keys
     * @return array
     */
    protected function order_keys($dictionary, $keys) {
        return array_map(function ($key) use ($dictionary) {
            return $dictionary[$key];
        }, $keys);
    }

    /**
     * Returns a formatted array containing evaluation details for the leaderboard
     *
     * @param int $offset the entries' offset
     * @return array
     */
    protected function format_entries($offset = 0) {
        $entries = [];
        foreach ($this->instance->entries() as $index => $entry) {

            $metadata = [];
            foreach ($this->filter_keys($entry, $this->instance->metadata()) as $key => $value) {
                $metadata[] = [
                    'key' => $key,
                    'info' => $this->instance->get_dimension($key),
                    'data' => (string) $value,
                    'missing' => !isset($value) || $value == '',
                ];
            }

            $columns = [];
            foreach ($this->filter_keys($entry, $this->instance->columns()) as $key => $value) {
                $columns[] = [
                    'key' => $key,
                    'data' => (string) $value,
                    'missing' => !isset($value) || $value == '',
                    'privacy_concern' => ($key == 'identifier' && isset($entry['auth_user']) && $entry['auth_user']), // todo grid object could store sensitive dimensions array
                ];
            }

            $entries[$index] = [
                'index' => $offset + $index + 1,
                'trophy' => $this->trophy_data($offset + $index),
                'picture' => isset($entry['picture']) ? $entry['picture'] : false,
                'columns' => $columns,
                'metadata' => $metadata,
            ];
        }
        return $entries;
    }

    protected function trophy_data($index) {
        switch ($index) {
            case 0:
                return ['color' => ['start' => '#f2ce5a', 'end' => '#9d7c1b'], 'icon' => 'fa-trophy'];
            case 1:
                return ['color' => ['start' => '#c9c8cb', 'end' => '#757070'], 'icon' => 'fa-trophy'];
            case 2:
                return ['color' => ['start' => '#a97142', 'end' => '#5a4f40'], 'icon' => 'fa-trophy'];
            default:
                return false;
        }
    }

    /**
     * Returns the table columns and metadata dimensions
     * @return array[]
     */
    protected function format_header() {
        return [
            'columns' => $this->order_keys($this->instance->dimensions(), $this->instance->columns()),
            'metadata' => $this->order_keys($this->instance->dimensions(), $this->instance->metadata()),
            'has' => [
                'columns' => count($this->instance->columns()) > 0,
                'metadata' => count($this->instance->metadata()) > 0,
            ],
        ];
    }

    /**
     * @inheritDoc
     */
    public function to_array() {
        return [
            'header' => $this->format_header(),
            'entries' => $this->format_entries($this->offset)
        ];
    }
}