<?php
// This file is part of VPL Code Challenges for Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * @author Photis Avrilionis
 * @copyright 2020 Photis Avrilionis
 *
 * This software was developed with the support of the following organizations:
 * - Université Grenoble Alpes
 * - Institut Polytechnique de Grenoble
 */

defined('MOODLE_INTERNAL') || die;

use mod_vplcc\factories\evaluations_factory;
use mod_vplcc\models\dimension;
use mod_vplcc\models\vplcc;
use mod_vplcc\models\content;

/**
 * Indicates if a given feature is supported by the module
 *
 * @param bool $feature
 * @return bool
 */
function vplcc_supports($feature) {
    switch ($feature) {
        case FEATURE_MOD_INTRO:
            return true;
        default:
            return false;
    }
}

/**
 * Creates a new VPL Code Challenge instance from the data provided by the moodle activity creation form.
 *
 * @param $form
 * @return mixed
 */
function vplcc_add_instance($form) {
    global $DB, $USER;

    $transaction = $DB->start_delegated_transaction();

    try {

        $vplcc = new vplcc([
            'name' => $form->name,
            'course' => $form->course,
            'intro' => $form->intro,
            'introformat' => $form->introformat,
            'starttimestamp' => $form->release,
            'vplcm' => $form->vplcm,
            'scope' => $form->scope,

            'columns' => $form->columns,
            'metadata' => $form->metadata,

            'usermodified' => $USER->id,
            'timecreated' => time(),
            'timemodified' => time()
        ]);

        $vplcc->save();

        $content = new content([
            'vplccid' => $vplcc->id,
            'instructions' => $form->instructions['text'],
            'instructionsformat' => $form->instructions['format'],
            'specifications' => $form->specifications['text'],
            'specificationsformat' => $form->specifications['format']
        ]);

        $content->save();

        if ($form->dimensions > 0) {
            foreach ($form->dimension as $key => $dimension) {
                if (!isset($form->delete_dimension[$key])) {
                    $newdim = new dimension([
                        'vplccid' => $vplcc->id,
                        'slug' => $dimension['slug'],
                        'name' => $dimension['name'],
                        'description' => $dimension['description'],
                    ]);

                    $newdim->save();
                }
            }
        }

        $transaction->allow_commit();

    } catch (Exception $e) {
        $transaction->rollback($e);
    }

    return $vplcc->id;
}

/**
 * Updates a VPL Code Challenge instance
 *
 * @todo update leaderboard dimensions details
 *
 * @param $form
 * @return bool
 */
function vplcc_update_instance($form) {
    global $DB, $USER;

    $transaction = $DB->start_delegated_transaction();

    try {

        $vplcc = vplcc::find_or_fail($form->instance);

        $vplcc->load([
            'name' => $form->name,
            'intro' => $form->intro,
            'introformat' => $form->introformat,
            'starttimestamp' => $form->release,
            'vplcm' => $form->vplcm,
            'scope' => $form->scope,

            'columns' => $form->columns,
            'metadata' => $form->metadata,

            'usermodified' => $USER->id,
            'timemodified' => time()
        ]);

        $vplcc->save();

        $content = content::find_or_fail([
            'vplccid' => $vplcc->id
        ]);

        $content->fill([
            'instructions' => $form->instructions['text'],
            'instructionsformat' => $form->instructions['format'],
            'specifications' => $form->specifications['text'],
            'specificationsformat' => $form->specifications['format']
        ]);

        $content->save();

        if ($form->dimensions > 0) {
            foreach ($form->dimension as $key => $dimension) {
                $dbdim = dimension::find([
                    'vplccid' => $vplcc->id,
                    'slug' => $dimension['slug']
                ]);

                if (isset($form->delete_dimension[$key]) && $form->delete_dimension[$key] == 1) {
                    if ($dbdim !== false) {
                        evaluations_factory::delete_dimension_facts($dbdim);
                        $dbdim->delete();
                    }
                } else {
                    if ($dbdim === false) {
                        // Create new dimension if current does not exist
                        $dbdim = new dimension([
                            'vplccid' => $vplcc->id,
                        ]);
                    }
                    $dbdim->fill([
                        'slug' => $dimension['slug'],
                        'name' => $dimension['name'],
                        'description' => $dimension['description'],
                    ]);
                    $dbdim->save();
                }
            }
        }

        $transaction->allow_commit();

        return true;

    } catch (Exception $e) {
        $transaction->rollback($e);
    }

    return false;
}

/**
 * Deletes a VPL Code Challenge instance and all it's dependents
 *
 * @param $id
 * @return bool
 */
function vplcc_delete_instance($id) {
    global $DB;

    $transaction = $DB->start_delegated_transaction();

    try {

        $vplcc = vplcc::find_or_fail($id);

        $content = content::find_or_fail([
            'vplccid' => $id
        ]);

        // TODO move mass db delete functions to generic ORM model factory (::flush($options)).

        // Delete VPL CC dimensions.
        $DB->delete_records('vplcc_dimensions', [
            'vplccid' => $id
        ]);

        // Delete VPL CC evaluation facts.
        $evaluations = $DB->get_recordset('vplcc_evaluations', [
            'vplccid' => $id
        ]);

        foreach ($evaluations as $evaluation) {
            $DB->delete_records('vplcc_evaluation_facts', [
                'vplccsubmissionid' => $evaluation->id
            ]);
        }

        // Delete VPL CC evaluations.
        $DB->delete_records('vplcc_evaluations', [
            'vplccid' => $id
        ]);

        // Delete VPL CC content and vplcc.
        $content->delete();
        $vplcc->delete();

        $transaction->allow_commit();

        return true;

    } catch (Exception $e) {
        $transaction->rollback($e);
    }

    return false;
}

/**
 * Extends the module settings menu
 *
 * @param settings_navigation $settings
 * @param navigation_node $vplccnode
 */
function vplcc_extend_settings_navigation(settings_navigation $settings, navigation_node $vplccnode) {
    global $PAGE;

    if (! empty($PAGE->cm->instance)) {
        $vplcc = vplcc::find($PAGE->cm->instance);
        $vplcontext = context_module::instance($vplcc->vplcm);

        if (!empty($vplcontext) && has_capability('moodle/course:manageactivities', $vplcontext)) {
            $node = $vplccnode->create(get_string('menu:editattached', 'vplcc'),
                new moodle_url('/course/modedit.php', ['update' => $vplcc->vplcm, 'return' => 1]),
                navigation_node::TYPE_SETTING,
                "",
                "attachedmodedit",
                new pix_icon('icon', '', 'mod_vpl')
            );

            $vplccnode->add_node($node, 'roleassign');
        }
    }
}

/**
 * Defines the reset form fields for VPL Code Challenges
 *
 * @param $mform
 */
function vplcc_reset_course_form_definition(&$mform) {
    $mform->addElement('header', 'vplccheader', get_string('modulenameplural', 'vplcc'));
    $mform->addElement('checkbox', 'reset_vplcc_all', get_string('messages:reset:all_entries', 'vplcc'));
}

/**
 * Defines the default values for the VPL Code Challenges reset form fields
 *
 * @param $course
 * @return int[]
 */
function vplcc_reset_course_form_defaults($course) {
    return [
        'reset_vplcc_all' => 0,
    ];
}

/**
 * Deletes all leaderboard entries for a course
 *
 * @todo detect if VPLs are being reset and flush leaderboard entries for those too
 *
 * @param $data
 * @return array
 */
function vplcc_reset_userdata($data) {

    $status = [];
    $componentstr = get_string('modulenameplural', 'vplcc');

    if (!empty($data->reset_vplcc_all)) {

        $vplccs = vplcc::all(['course' => $data->id]);

        foreach ($vplccs as $vplcc) {

            $current = [
                'component' => $componentstr,
                'item' => get_string('messages:reset:flushed_vplcc', 'vplcc', $vplcc->name),
                'error' => false
            ];

            try {
                evaluations_factory::delete_evaluations($vplcc);
            } catch (Exception $e) {
                $current['error'] = get_string('messages:reset:could_not_delete', 'vplcc');
            }

            $status[] = $current;
        }
    }

    return $status;
}
