<p align="center"><a href="https://pavril.github.io/vpl-code-challenges-docs" target="_blank"><img src="https://user-images.githubusercontent.com/6431229/111039768-090bd880-8430-11eb-9411-ebe196e3cc0e.png" width="750"></a></p>

<!-- The emojis by Twitter, Inc used in the cover are licensed under CC-BY 4.0. See more here: https://twemoji.twitter.com/ -->

<p align="center">
  <a href="https://github.com/pavril/vpl-code-challenges/actions/workflows/moodle-ci.yml"><img src="https://github.com/pavril/vpl-code-challenges/actions/workflows/moodle-ci.yml/badge.svg" alt="Build Status"></a>
  <a href="https://github.com/pavril/vpl-code-challenges/blob/master/LICENSE.md"><img src="https://img.shields.io/github/license/moodle/moodle" alt="License"></a>
  <a href="#"><img src="https://img.shields.io/badge/Moodle-3.8%2B-blue" alt="Moodle 3.8+"/></a>
  <a href="https://caseine.org"><img src="https://img.shields.io/badge/A%20caseine.org%20project-plastic--?color=gray&logo=data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAABmJLR0QA/wD/AP+gvaeTAAAACXBIWXMAABG3AAARtwGaY1MrAAAFr0lEQVRYw61XW2wUVRj+zpnZ2Zm9sN1u2V4opTSGAmkCoRQEIpoQifAgEmNEjYEHEx5UgoHIC5A+SHxSY0xIUEtiYki8BMU0wRCQNFJssZGCcrNWbLulu91t99admZ2rD6Xs7sx0F5T/7Zzzz/9/5/sv5x+CxyDxeLwBQDuAtXnD2EiANTdSqe+2tra+Uelb9lGdJZPJKtM020zTbM9r2noDeJoQ0jB3zjMMACCazT75MPbIwzoej0ZfoZR+6KK0znpmAojmcoiLInKqiryqIpJMTu0fHq7H3r3qY2HgeiLx8rJgsC4mipiWJCi6DpYQ+FwuNHi98LMs/H4/VMNAd18fAqIYOhwOX3gP2Py/Gejq7T2wJhx+PygIrnJ6iq6ju78f3mQSAODy+9FH6bFPd+06/J8A/HjjRnVOlrvbw+ENIOWxKpqG7itXHjifEy4cNs6k09u+37Pn3CMB+LK//9XlwWBXjcfDV2IoIYr46eJFyLEYeJ4Hz/PgOA7kPmjS2CgfHR5uiu7bF68IYGBgwDMMfNMRDm+nlAIA8rqOaUlCSlEgaRoIIXCzLKp4Hg1eL3iWRWR0FKphIJPPIydJUEQRiqKAqCpgGIj5/ZGDg4NL0NlpzAvg9LVrbaqinOdcrlo3w0Dg3BA4AU/4hVmaTYBz4EzRddwbGyvLkmGaOBOJfHFs5849xfu0eMGa5qYtS5fWPtXYiHX19VBcfpxLGvjonxnEFQOH7qSRVA2bcdOyvprIYjQr4W5GKjgiBGMzM17rtyUATEpLrDcLDL6KSrg+o+GHSRkdAQ4XphXH2xXLUCqHvlgGybyKhFzQp5Tayr50gxC9eBlyUbwQ5lHNMfjg7gwkw0RXW1VFAAs4FgOTGURmZGxbUoManpu7bXkAhqbZ+H2n2QcAeL1ewPNXp7E5yFUE8FxTDTbWVeH035NYHiywTgmxASgNgWnaA3xfDtzJoM5NYZj2MysAANBNYFpWS7KcAExZBkyW1eYDIDAEW2sEUIcq0B0AACaWBz1WZ+UZ0DTNZkm+f+VlHgan7olQnCiwiGoYcFGKobRUWiEVQ2AYNgZ2/57CvbyO1+oF7F7kfQCoXAjOjiQg6zpebAmj+IgSYguBNQds1kclHZ9HRLx9K42Dd9IYFnWrig2AqBnonUjhs5vjmFELd2Iq5gAhNga62gJY6Zt9BI8OZbCIt9mAFfeOljAEhiLEu+BzFVxQSssD0C2NCABW+lw4MZbDQEZDnZsirugIcyXE2ZJQYCiO/zEGjlKsCvnh52bdkEpJqKuqnV8A2xfyiOY1nIzkkLXnKRwih2cXhxCXFdCisqGEUKteKQOG4dgHPhnJ4dvVIXwdldAs2Icop7q4lcxhWcCDrKLBy84yz1RKQn2eRtQRcGHrwBR008SRoQz+zJWmihMDDR4OK6t9uBJLP9hzakSllFjegjl5qU5Az7oQvAzBuy0+cJZu5NSIWqu8mMjlsaUxVLxdoQznATAi6zg1IeH4aA6yMftKVmKgfzKNwUQWAltwwc5NOPMByCuKYwgaOIqe6TyiiuE4wzm9BRvrqpDKq5gQS57vCgxQ6siAixJ8vCKAt5q8mMjbVazuJyUFHpbB4bUtYIoQMw5VULKhaFqJdcMETozlYALgKcGbTV78nLQPJNYQ3M1IGMnK0EwTt5O5grNKVQBLGVICtHpZ7L+dQU6fdXI14zARWdbrawMYTGRwdiQBjqHFzsr3AcMhCZ+pdqORZ7D/VhoMAVYvsA8kTkm4Y2kYMVHBQqGgLxuGzX4JgEM9Pb/1RqMdzX7/hhDPrwpwXGu1ICwOud01R5b4PY0+H6EOPyhOAKbyeTMizaR/nRL/SijKL7czmdPnx8cvWfUe+ucUADpOnmxpDwY3hTyeNdWCsKKK45pDPF8ra5onEY9LE7I8FJPlyzdTqTOXY7FL6OxUKtn8Fx7xfUhncWHmAAAAJXRFWHRkYXRlOmNyZWF0ZQAyMDIxLTAzLTEzVDE4OjIyOjMxKzAwOjAwpeGsmgAAACV0RVh0ZGF0ZTptb2RpZnkAMjAyMS0wMy0xM1QxODoyMjozMSswMDowMNS8FCYAAAAZdEVYdFNvZnR3YXJlAHd3dy5pbmtzY2FwZS5vcmeb7jwaAAAAAElFTkSuQmCC" img="A caseine.org project"></a>
</p>

## About VPL Code Challenges

VPL Code Challenges is a Moodle activity plugin that allows you to create code challenge activities for your students. The plugin uses VPL activities to evaluate the responses of challenge players, and creates a leaderboard dynamically based on their execution logs.

Documentation and installation guides available in the project documentation website.

## Supported by

The VPL Code Challenges plugin was developed with the support of the [University of Grenoble Alpes](https://www.univ-grenoble-alpes.fr/) and the [Grenoble Institute of Technology (Grenoble INP)](https://www.grenoble-inp.fr/).

## Contributing

Thank you for considering contributing to the project! The contribution guide can be found in the [contributing](CONTRIBUTING.md) file.

## License

The VPL Code Challenges Moodle plugin is open-sourced software licensed under the [GNU General Public License v3.0](https://www.gnu.org/licenses/gpl-3.0.html).
