<?php
// This file is part of VPL Code Challenges for Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * @author Photis Avrilionis
 * @copyright 2020 Photis Avrilionis
 *
 * This software was developed with the support of the following organizations:
 * - Université Grenoble Alpes
 * - Institut Polytechnique de Grenoble
 */

namespace mod_vplcc\controllers;

defined('MOODLE_INTERNAL') || die();

use mod_vplcc\core\http\base_controller as controller;
use mod_vplcc\core\routing\url;
use mod_vplcc\factories\evaluations_factory;

class base_controller extends controller {

    protected function navigation_data($cm, $active = null) {
        $data = [
            'description_url' => url::route('view', ['id' => $cm->id])->out(false),
            'leaderboard_url' => url::route('leaderboard.view', ['id' => $cm->id])->out(false),
            'lastupdate' => evaluations_factory::get_last_update($cm->instance),
        ];

        if ($active !== null) {
            $data[$active . "_active"] = true;
        }

        return $data;
    }

    protected function feedback_data() {
        return [
            'show' => get_config('mod_vplcc', 'show_feedback'),
            'form' => false,
            'discussion' => 'https://im2ag-mattermost.univ-grenoble-alpes.fr/stagiaires/channels/challenges',
            'issues' => 'https://gricad-gitlab.univ-grenoble-alpes.fr/avriliop/caseine-code-challenges/-/issues',
            'wiki' => 'https://pavril.github.io/vpl-code-challenges-docs/',
        ];
    }

}
