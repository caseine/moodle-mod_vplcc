<?php
// This file is part of VPL Code Challenges for Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * @author Photis Avrilionis
 * @copyright 2020 Photis Avrilionis
 *
 * This software was developed with the support of the following organizations:
 * - Université Grenoble Alpes
 * - Institut Polytechnique de Grenoble
 */

namespace mod_vplcc\core\database;

defined('MOODLE_INTERNAL') || die();

trait has_attributes_trait {

    /**
     * The model's attributes
     *
     * @var array
     */
    protected $attributes = [];

    /**
     * The model's mass assignable attributes
     *
     * Fillable attributes can always be mass assigned, regardless of
     * the model's guard state.
     *
     * @var array
     */
    protected $fillable = [];

    /**
     * The model's hidden attributes
     *
     * Hidden attributes are not included in model representations.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * The model's guard state
     *
     * The guard state indicates whether attributes can be mass assigned
     * or not. When the model is guarded, only attributes defined in `$fillable`
     * can be mass assigned.
     *
     * @var boolean
     */
    private $guarded = true;

    /**
     * The original model attributes, as retrieved from the database.
     *
     * @var array
     */
    private $original = [];

    /**
     * The modified model attributes.
     *
     * @var array
     */
    private $changes = [];

    /**
     * Retrieves the model's attributes
     */
    public function get_attributes() {
        return $this->attributes;
    }

    /**
     * Retrieves the model's fillable attributes
     */
    public function get_fillable() {
        return $this->fillable;
    }

    /**
     * Retrieves the model's hidden attributes
     */
    public function get_hidden() {
        return $this->hidden;
    }

    /**
     * Retrieves the model's original values
     */
    public function get_originals() {
        return $this->original;
    }

    /**
     * Retrieves the model's changed values
     */
    public function get_changes() {
        return $this->changes;
    }

    /**
     * Indicates whether changes to a model attribute are not made persistent.
     *
     * An attribute is made persistent once saved in the database.
     *
     * @return boolean
     */
    public function is_dirty() {
        foreach ($this->get_attributes() as $key) {
            if ($this->is_changed($key)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Indicates whether the model is guaded.
     *
     * @return boolean
     */
    public function is_guarded() {
        return $this->guarded;
    }

    /**
     * Modifies the guard state.
     *
     * When the model is guarded, only fillable attributes can
     * be modified.
     *
     * @param bool $guarded
     * @return $this
     */
    public function set_guarded($guarded) {
        $this->guarded = $guarded;
        return $this;
    }

    /**
     * Indicates if the attribute can be filled.
     *
     * An attribute is fillable if the model is unguarded, or if it
     * or if it is set as fillable.
     *
     * @param string $key
     * @return boolean
     */
    public function is_fillable($key) {
        return $this->is_guarded() ? in_array($key, $this->get_fillable()) : true;
    }

    /**
     * Indicates whether a model attribute is hidden.
     *
     * @param string $key
     * @return boolean
     */
    public function is_hidden($key) {
        return in_array($key, $this->hidden);
    }

    /**
     * Indicates whether a model attribute is visible.
     *
     * @param string $key
     * @return boolean
     */
    public function is_visible($key) {
        return !$this->is_hidden($key);
    }

    /**
     * Indicates whether a model attribute is visible.
     *
     * @return boolean
     */
    public function is_changed($key) {
        return isset($this->changes[$key]);
    }

    /**
     * Indicates whether a model has an attribute.
     *
     * @param string $key
     * @return boolean
     */
    public function has_attribute($key) {
        return in_array($key, $this->get_attributes());
    }

    /**
     * Retrieves the value for a model attribute if it exists.
     *
     * @param string $key
     * @return mixed
     */
    public function get_attribute($key) {
        if ($this->has_attribute($key)) {
            return $this->get_attribute_value($key);
        }
        return;
    }

    /**
     * Retrieves the original value for an attribute on the model
     *
     * @param string $key
     * @return mixed
     */
    protected function get_original_attribute_value($key) {
        return isset($this->get_originals()[$key]) ? $this->get_originals()[$key] : null;
    }

    /**
     * Retrieves the changed value for an attribute on the model
     *
     * @param string $key
     * @return mixed
     */
    protected function get_changed_attribute_value($key) {
        return isset($this->get_changes()[$key]) ? $this->get_changes()[$key] : null;
    }

    /**
     * Retrieves the value for a model attribute.
     *
     * Modified values precede the original ones.
     *
     * @param string $key
     * @return mixed
     */
    protected function get_attribute_value($key) {
        return $this->is_changed($key) ? $this->get_changed_attribute_value($key) : $this->get_original_attribute_value($key);
    }

    /**
     * Sets the value of an attribute if it exists and can be filled.
     *
     * @param string $key
     * @param mixed $value
     * @return $this
     */
    public function set_attribute($key, $value) {
        if ($this->has_attribute($key) && $this->is_fillable($key)) {
            $this->set_attribute_value($key, $value);
        }
        return $this;
    }

    /**
     * Sets the value for a model attribute.
     *
     * @param string $key
     * @param mixed $value
     */
    protected function set_attribute_value($key, $value) {
        if ($this->get_original_attribute_value($key) !== $value) {
            $this->changes[$key] = $value;
        }
    }

    /**
     * Merges modified attributes with original values
     *
     * @todo rename method to differentiate it from database-related operations (i.e. persistent data)
     * @return $this
     */
    public function persist() {
        foreach ($this->get_changes() as $key => $value) {
            $this->original[$key] = $value;
        }
        $this->reset();
        return $this;
    }

    /**
     * Merges original attributes with changed values.
     *
     * Previously changes attributes take precedence over originals.
     *
     * @todo rename method to differentiate it from database-related operations (i.e. persistent data)
     * @return $this
     */
    public function unpersist() {
        foreach ($this->get_originals() as $key => $value) {
            if (! $this->is_changed($key)) {
                $this->changes[$key] = $value;
            }
        }
        $this->original = [];
        return $this;
    }

    /**
     * Flushes all changes made to model attributes
     *
     * @return $this
     */
    public function reset() {
        $this->changes = [];
        return $this;
    }

    /**
     * Fills model attributes.
     *
     * Overloading: function accepts stdClasses (returned from Moodle database calls),
     * arrays or json strings.
     *
     * For json string, it's also possible to specify the json behaviour constraint
     * and recursion depth that are used when decoding the string.
     *
     * @param object|array|string $attributes
     * @param int $options json behaviour constraint
     * @param int $depth json recursion depth
     * @return $this
     */
    public function fill($attributes = [], $options = 0, $depth = 512) {

        if (is_object($attributes)) {
            // Overloading: moodle data object.
            $attributes = get_object_vars($attributes);
        } else if (is_string($attributes)) {
            // Overloading: attributes array.
            $attributes = json_decode($attributes, true, $depth, $options);
        }

        foreach ($attributes as $key => $value) {
            $this->set_attribute($key, $value);
        }

        return $this;
    }

    /**
     * Force-fills model attributes.
     *
     * Fills the attributes while disabling guard mode.
     *
     * @param mixed $attributes
     * @return $this
     */
    public function load($attributes = [], $options = 0, $depth = 512) {
        return $this->set_guarded(false)->fill($attributes, $options, $depth)->set_guarded(true);
    }

    /**
     * Dynamically retrieves attributes on the model.
     *
     * @param string $key
     * @return mixed
     */
    public function __get($key) {
        return $this->get_attribute($key);
    }

    /**
     * Dynamically sets attributes on the model.
     *
     * @param  string  $key
     * @param  mixed  $value
     * @return void
     */
    public function __set($key, $value) {
        $this->set_attribute($key, $value);
    }
}
