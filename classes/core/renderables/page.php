<?php
// This file is part of VPL Code Challenges for Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * @author Photis Avrilionis
 * @copyright 2020 Photis Avrilionis
 *
 * This software was developed with the support of the following organizations:
 * - Université Grenoble Alpes
 * - Institut Polytechnique de Grenoble
 */

namespace mod_vplcc\core\renderables;

defined('MOODLE_INTERNAL') || die();

use renderable;
use templatable;
use renderer_base;
use mod_vplcc\core\http\response;
use mod_vplcc\core\contracts\http\responsable;

/**
 * Class page
 *
 * @todo move response construction to factory: a renderable should be agnostic of the $PAGE global
 *   that's required to create a response.
 *
 * @package mod_vplcc\core\renderables
 */
class page implements renderable, templatable, responsable {

    /**
     * The data that's passed to the template
     *
     * @var array
     */
    protected $data;

    /**
     * The template used to render the page
     * @var array
     */
    protected $template;

    public function __construct($template, $data = []) {
        $this->template = $template;
        $this->data = $data;
    }

    /**
     * Export this data so it can be used as the context for a mustache template.
     *
     * @param renderer_base $output
     * @return array
     */
    public function export_for_template(renderer_base $output) {
        return $this->data;
    }

    public function template() {
        return $this->template;
    }

    /**
     * {@inheritDoc}
     */
    public function to_response() {
        return new response($this, 'mod_vplcc');
    }
}
