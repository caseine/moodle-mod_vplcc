<?php
// This file is part of VPL Code Challenges for Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * @author Photis Avrilionis
 * @copyright 2020 Photis Avrilionis
 *
 * This software was developed with the support of the following organizations:
 * - Université Grenoble Alpes
 * - Institut Polytechnique de Grenoble
 */

namespace mod_vplcc\core\database;

defined('MOODLE_INTERNAL') || die();

use mod_vplcc\core\contracts\support\arrayable;
use mod_vplcc\core\contracts\support\jsonable;

abstract class model implements arrayable, jsonable {

    use queries_database_trait;
    use has_attributes_trait;

    /**
     * The database table name associated with the model
     *
     * @var string
     */
    protected $table;

    /**
     * The associated table's primary key column name
     *
     * @var string
     */
    protected $primarykey = "id";

    /**
     * Indicates whether the model exists in the database.
     *
     * @var bool
     */
    private $exists = false;

    /**
     * Initialises the model
     *
     * @param bool $exists
     * @param array $attributes
     */
    private function init($exists, $attributes) {
        $this->set_exists($exists);
        $this->load($attributes);
        if ($this->exists()) {
            $this->persist();
        }
    }

    /**
     * Creates a new model instance and fills attributes
     *
     * @param array $attributes
     */
    public function __construct($attributes = []) {
        $this->init(false, $attributes);
    }

    /**
     * Create a model for an existing database entry.
     *
     * @param int|array $options
     * @param int $strictness
     * @return model|bool
     */
    public static function find($options = [], $strictness = IGNORE_MISSING) {
        $model = new static();

        // Overloading: if $options is not an array, then we consider it's
        // the object's id. We call the constructor passing the primary key
        // as an option.

        if (!is_array($options)) {
            return static::find([
                $model->get_primary_key() => $options
            ], $strictness);
        }

        $attributes = self::db()->get_record($model->table, $options, '*', $strictness);

        if ($attributes == false) {
            return false;
        }

        $model->init(true, $attributes);

        return $model;
    }

    /**
     * Create a model for an existing database entry or fails.
     *
     * @param int|array $options
     * @return model
     */
    public static function find_or_fail($options = []) {
        return static::find($options, MUST_EXIST);
    }

    public static function all($options = [], $sort='', $limitfrom=0, $limitnum=0) {
        $base = new static();

        $models = [];

        $rs = self::db()->get_recordset($base->table, $options, $sort, '*', $limitfrom, $limitnum);

        if (! $rs->valid()) {
            $rs->close();
            return false;
        }

        foreach ($rs as $record) {
            $model = new static();
            $model->init(true, $record);
            $models[$model->get_attribute($model->get_primary_key())] = $model;
        }

        $rs->close();
        return $models;
    }

    /**
     * Returns the associated table's primary key column name
     * @return string
     */
    public function get_primary_key() {
        return $this->primarykey;
    }

    /**
     * Indicates whether changes to a model exists in the database.
     *
     * @return boolean
     */
    public function exists() {
        return $this->exists;
    }

    /**
     * Sets the exist state.
     *
     * @param bool
     * @return $this
     */
    private function set_exists($exists) {
        $this->exists = $exists;
        return $this;
    }

    /**
     * Saves the model.
     *
     * Makes changes persistent by storing the modified attributes in
     * the database.
     *
     * @todo automate usermodified, timemodified and timeupdated values when model is saved
     *
     * @return bool
     */
    public function save() {
        $saved = false;

        if ($this->exists()) {
            // If the model exists, only make its modified attributes persistent.
            if ($this->is_dirty()) {
                $changes = (object) $this->get_changes();
                $changes->id = $this->get_attribute($this->get_primary_key());
                $saved = self::db()->update_record($this->table, $changes);
            }
        } else {
            // If the model does not exists, create a new database entry.
            $id = self::db()->insert_record_raw($this->table, (object) $this->get_changes());
            if ($id !== false) {
                $this->load([$this->get_primary_key() => $id])->set_exists(true);
                $saved = true;
            }
        }

        if ($saved) {
            $this->persist();
        }
        return $saved;
    }

    /**
     * Deletes the model from the database
     *
     * @return bool
     */
    public function delete() {

        // Model must exist to be deleted
        if (! $this->exists()) {
            return false;
        }

        $deleted = self::db()->delete_records($this->table, [
            $this->get_primary_key() => $this->get_attribute($this->get_primary_key()),
        ]);

        $deleted = true;

        if ($deleted) {
            $this->unpersist()->set_exists(false);

            /*
             * @todo implement model attribute unsetting
             * note: unsetting a changes item breaks the attributes trait encapsulation. The
             * changes attribute should only be modified by methods implemented inside the trait.
             */
            unset($this->changes[$this->get_primary_key()]);
        }

        return $deleted;
    }

    /**
     * Returns an array representation of the model.
     *
     * @param bool $withhidden indicates if hidden attributes should be included
     * @return array
     */
    public function array_representation($withhidden = false) {
        $model = [];
        foreach ($this->get_attributes() as $key) {
            if ($withhidden || $this->is_visible($key)) {
                $model[$key] = $this->get_attribute($key);
            }
        }
        return $model;
    }

    /**
     * Returns an array representation of the model.
     * {@inheritDoc}
     * @see \mod_vplcc\core\contracts\support\arrayable::to_array()
     */
    public function to_array() {
        return $this->array_representation();
    }

    /**
     * Returns a json representation of the model.
     * {@inheritDoc}
     * @see \mod_vplcc\core\contracts\support\jsonable::to_json()
     */
    public function to_json($options = 0) {
        return json_encode($this->array_representation(), $options);
    }

    /**
     * Returns a moodle dataobject representation of the model.
     *
     * @link https://docs.moodle.org/dev/Data_manipulation_API
     */
    public function to_dataobject() {
        return (object) $this->array_representation();
    }

}
