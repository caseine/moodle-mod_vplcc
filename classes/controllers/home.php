<?php
// This file is part of VPL Code Challenges for Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * @author Photis Avrilionis
 * @copyright 2020 Photis Avrilionis
 *
 * This software was developed with the support of the following organizations:
 * - Université Grenoble Alpes
 * - Institut Polytechnique de Grenoble
 */

namespace mod_vplcc\controllers;

defined('MOODLE_INTERNAL') || die();

use context_module;
use context_course;
use mod_vpl;
use mod_vplcc\core\routing\url;
use mod_vplcc\core\renderables\page;
use mod_vplcc\models\vplcc;
use mod_vplcc\models\content;

class home extends base_controller {

    public function redirect() {
        $id = required_param('id', PARAM_INT);
        redirect(url::route('view', ['id' => $id]));
    }

    /**
     * Lists code challenges instances for the current course
     * Replicates Moodle's default behaviour: https://docs.moodle.org/dev/Activity_modules#view.php)
     *
     * @todo include attached activity info (name, description, link, icon, ...)
     */
    public function index() {
        global $PAGE, $DB;

        $id = required_param('id', PARAM_INT);

        if (!$course = $DB->get_record('course', ['id' => $id])) {
            print_error('coursemisconf');
        }

        require_course_login($course);

        if (! $cms = get_coursemodules_in_course('vplcc', $course->id)) {
            print_error('invalidcoursemodule');
        }

        $PAGE->set_context(context_course::instance($course->id));
        $PAGE->set_pagelayout('incourse');

        $PAGE->set_title(get_string('index:title', 'vplcc'));
        $PAGE->set_heading(get_string('index:heading', 'vplcc'));

        return new page('mod_vplcc/pages/index', [
            'feedback' => $this->feedback_data(),
            'course_modules' => array_values(array_map(function ($cm) {
                return [
                        'id' => $cm->id,
                        'name' => $cm->name,
                        'added' => $cm->added,
                        'url' => url::route('view', ['id' => $cm->id])->out(false)
                ];
            }, $cms)),
        ]);
    }

    public function view() {
        global $PAGE, $DB, $USER;

        $id = required_param('id', PARAM_INT);

        if (! $cm = get_coursemodule_from_id('vplcc', $id)) {
            print_error('invalidcoursemodule');
        }

        if (! $course = $DB->get_record("course", ["id" => $cm->course])) {
            print_error('coursemisconf');
        }

        require_login($course, true, $cm);

        $vplcc = vplcc::find($cm->instance);

        if (! $vplcc) {
            print_error('invalidcoursemodule');
        }

        $content = content::find(['vplccid' => $vplcc->id]);

        $vpl = new mod_vpl($vplcc->vplcm);

        $PAGE->set_context(context_module::instance($cm->id));
        $PAGE->set_pagelayout('incourse');

        $PAGE->set_title($vplcc->name);
        $PAGE->set_heading($vplcc->name);

        $group = $vpl->get_usergroup($USER->id);

        return new page('mod_vplcc/pages/view', [
            'feedback' => $this->feedback_data(),
            'navigation' => $this->navigation_data($cm, 'description'),
            'activity_url' => url::route('activity', ['cm' => $vplcc->vplcm])->out(false),
            'cm' => $cm,
            'course' => $course,
            'vplcc' => $vplcc->to_array(),
            'vpl' => [
                'instance' => $vpl->get_instance(),
                'pix_module' => 'mod_vpl',
                'groups' => [
                    'enabled' => $vpl->is_group_activity(),
                    'current_group' => ($group !== false ? $group->name : false),
                ],
            ],
            'content' => $content->to_array()
        ]);
    }
}
