// This file is part of VPL Code Challenges for Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * @author Photis Avrilionis
 * @copyright 2020 Photis Avrilionis
 *
 * This software was developed with the support of the following organizations:
 * - Université Grenoble Alpes
 * - Institut Polytechnique de Grenoble
 */

window.requirejs.config({
    "paths": {
        "selectize": M.cfg.wwwroot + '/mod/vplcc/js/selectize.js-0.12.4/selectize.min',
    },
    "shim": {
        'selectize': {exports: 'selectize'},
    }
});

define(['jquery', 'jqueryui', 'selectize'], function($) {
    return {
        init: function() {
            $(function() {

                const sep = ',';

                $('[data-doggle=selectize]').each(function() {

                    let self = $(this);
                    let name = self.attr('name');
                    let hidden = name.substr(0, name.indexOf('_'));
                    let hidden_field = $("[name='" + hidden + "']");

                    let initial = hidden_field.val();

                    $.each(initial.split(sep), function (index, value) {
                        self.append('<option value="' + value + '" selected="true">' + value + '</option>');
                    });

                    self.selectize({
                        create: true,
                        delimiter: ',',
                        persist: false,
                        createOnBlur: true,
                        placeholder: self.attr('placeholder'),
                        selectOnTab: true,
                        loadingClass: 'disabled',
                        plugins: ['drag_drop', 'remove_button'],
                        onChange: function (value) {
                            hidden_field.val(value.join(sep));
                        }
                    });

                });

            });
        }
    };
});