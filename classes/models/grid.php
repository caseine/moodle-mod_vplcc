<?php
// This file is part of VPL Code Challenges for Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * @author Photis Avrilionis
 * @copyright 2020 Photis Avrilionis
 *
 * This software was developed with the support of the following organizations:
 * - Université Grenoble Alpes
 * - Institut Polytechnique de Grenoble
 */

namespace mod_vplcc\models;

defined('MOODLE_INTERNAL') || die();

use mod_vplcc\core\contracts\support\arrayable;

class grid implements arrayable {

    /**
     * The dataset's dimensions
     * @var array[]
     */
    protected $dimensions;

    /**
     * The dataset's entries
     * @var array[]
     */
    protected $entries;

    /**
     * The dataset's visible dimensions keys
     * @var string[]
     */
    protected $columns;

    /**
     * The dataset's metadata dimensions keys
     * @var string[]
     */
    protected $metadata;

    /**
     * grid constructor
     */
    public function __construct() {
        $this->dimensions = [];
        $this->entries = [];
        $this->columns = [];
        $this->metadata = [];
    }

    /**
     * Defines a dimension on the dataset
     *
     * @param string $key
     * @param string $name
     * @param string $description
     * @return $this
     */
    public function set_dimension($key, $name, $description = null) {
        $this->dimensions[$key]['name'] = $name;

        if ($description !== null) {
            $this->dimensions[$key]['description'] = $description;
        }

        return $this;
    }

    /**
     * Retrieves the dataset's dimensions
     *
     * @return array[]
     */
    public function dimensions() {
        return $this->dimensions;
    }

    public function get_dimension($key) {
        return $this->dimensions[$key];
    }

    /**
     * Sets the visible dataset columns.
     *
     * The order in which the columns are given is preserved when the dataset is exported.
     *
     * @param array $keys the dimensions keys
     * @return $this|string[]
     */
    public function set_columns($keys) {
        $this->columns = $keys;
        return $this;
    }

    public function columns() {
        return $this->columns;
    }

    /**
     * Sets the metadata dataset columns.
     *
     * @param array $attributes
     * @return $this
     */
    public function set_metadata($attributes) {
        $this->metadata = $attributes;
        return $this;
    }

    public function metadata() {
        return $this->metadata;
    }

    /**
     * Inserts an entry into the dataset
     *
     * @param array $entry the entry's values in the same order that the grid's attributes were defined
     * @return bool|grid
     */
    public function add_entry($entry) {
        if (array_keys($entry) !== range(0, count($entry) - 1)) {
            $this->entries[] = array_replace(array_fill_keys(array_keys($this->dimensions), null), $entry);

        } else {
            $this->entries[] = array_combine(array_keys($this->dimensions), $entry);
        }

        return $this;
    }

    /**
     * Returns the number of total entries in the dataset
     *
     * @return int
     */
    public function count() {
        return count($this->entries());
    }

    /**
     * Retrieves all grid entries
     *
     * @return array[]
     */
    public function entries() {
        return $this->entries;
    }

    /**
     * Sorts the available entries by a dimension key or using a custom sorter function
     *
     * @param string|callable $key
     * @param string $order
     * @return grid
     */
    public function sort($key, $order = 'asc') {
        if (is_callable($key)) {
            usort($this->entries, $key);
            return $this;
        }

        usort($this->entries, function($right, $left) use ($key, $order) {
            if ($right[$key] == $left[$key]) {
                return 0;
            }

            if ($order == 'asc') {
                return $right[$key] < $left[$key] ? -1 : 1;
            } else if ($order == 'desc') {
                return $right[$key] > $left[$key] ? -1 : 1;
            }
        });

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function to_array() {
        return $this->entries();
    }
}
