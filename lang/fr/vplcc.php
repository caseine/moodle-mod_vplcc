<?php
// This file is part of VPL Code Challenges for Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * @author Photis Avrilionis
 * @copyright 2020 Photis Avrilionis
 *
 * This software was developed with the support of the following organizations:
 * - Université Grenoble Alpes
 * - Institut Polytechnique de Grenoble
 */

// Generic.
$string['vplcc:addinstance'] = 'Ajouter un nouveau défi de code VPL';
$string['vplcc:view'] = 'Voir le défi de code VPL';
$string['modulename'] = 'Défi de code VPL';
$string['modulename_help'] = '<p>L\'activité défis de code VPL vous permet de créer des défis de code personnalisables à partir de Laboratoires virtuels de programmation (VPL) existants.</p><p><a target="_blank" href="https://pavril.github.io/vpl-code-challenges-docs/#/advanced/configuration-kitchen-sink">Aide à la configuration des défis de code (anglais)</a></p><p><a  target="_blank" href="https://pavril.github.io/vpl-code-challenges-docs/">Documentation du plugin (anglais)</a></p>';
$string['modulenameplural'] = 'Défis de code VPL';
$string['privacy:metadata'] = 'Le plugin VPL Code Challenge enregistre uniquement les identifiants des utilisateurs ou des groupes d\'utilisateurs qui déclenchent une évaluation sur une activité VPL liée à un défi de code.';
$string['pluginadministration'] = 'Gestion des défis de code VPL';
$string['pluginname'] = 'Défi de code VPL';
$string['search:activity'] = 'Défi de code VPL';
$string['menu:editattached'] = 'Paramètres de l\'activité attachée';

// Module form strings.
$string['mod_form:content'] = "Sommaire";
$string['mod_form:instructions'] = "Instructions";
$string['mod_form:specifications'] = "Spécifications";
$string['mod_form:availability'] = "Accès";
$string['mod_form:leaderboard'] = "Classement";
$string['mod_form:section:vplcm'] = "Configuration de l'activité VPL";
$string['mod_form:vplcm:activity'] = "Activité VPL attachée";
$string['mod_form:vplcm:activity:empty'] = "Il n'y a pas d'activités VPL dans ce cours.";
$string['mod_form:vplcm:activity:select'] = "Sélectionnez une activité VPL ...";
$string['mod_form:vplcm:activity:used_affix'] = "(utilisée)";
$string['mod_form:section:leaderboard'] = "Configuration du classement";
$string['mod_form:leaderboard:release'] = "Date de publication";
$string['mod_form:leaderboard:scope'] = "Restriction de la portée";
$string['mod_form:leaderboard:scope:none'] = "Aucune";
$string['mod_form:leaderboard:scope:attached'] = "Utiliser le groupement de l'activité attachée";
$string['mod_form:leaderboard:scope:groupings'] = "Groupements";
$string['mod_form:leaderboard:header:title'] = "Dimensions du classement";
$string['mod_form:leaderboard:header:tag'] = "Avancé";
$string['mod_form:leaderboard:header:lead'] = "Déclarer les dimensions générées lors des évaluations VPL pour les ajouter dans le classement.";
$string['mod_form:leaderboard:header:docs'] = "En savoir plus sur les dimensions";
$string['mod_form:leaderboard:header:dim:id'] = "Dimension de l'identifiant";
$string['mod_form:leaderboard:header:dim:id:name'] = "Nom/Groupe";
$string['mod_form:leaderboard:header:dim:id:desc'] = "Aucune description";
$string['mod_form:leaderboard:header:dim:grade'] = "Dimension de la note";
$string['mod_form:leaderboard:header:dim:grade:name'] = "Note";
$string['mod_form:leaderboard:dim:slug'] = "Slug";
$string['mod_form:leaderboard:dim:name'] = "Nom";
$string['mod_form:leaderboard:dim:desc'] = "Description";
$string['mod_form:leaderboard:dim:label'] = "Dimension {no}";
$string['mod_form:leaderboard:dim:delete'] = "Supprimer la dimension {no} lors de la sauvegarde";
$string['mod_form:leaderboard:dim:add'] = "Ajouter une nouvelle dimension";
$string['mod_form:leaderboard:columns'] = "Colonnes";
$string['mod_form:leaderboard:columns:placeholder'] = "Définir les dimensions à afficher sous forme de colonne";
$string['mod_form:leaderboard:meta'] = "Détails";
$string['mod_form:leaderboard:meta:placeholder'] = "Définir les dimensions à afficher sous forme de détails";

// Errors.
$string['error:mod_form:required'] = "Ce champ est obligatoire.";
$string['error:mod_form:missingvpl'] = "Vous devez choisir une activité VPL";
$string['error:mod_form:notfoundvpl'] = "L'activité VPL sélectionnée n'existe pas.";
$string['error:mod_form:usedvpl'] = "L'activité VPL sélectionnée est utilisée par un autre défi de code.";
$string['error:mod_form:dim:slug:missing'] = "Le slug est requis.";
$string['error:mod_form:dim:slug:notalpha'] = "Le slug doit être une chaîne alphanumérique.";
$string['error:mod_form:dim:slug:toolong'] = "Le slug doit comporter moins de 50 caractères.";
$string['error:mod_form:dim:slug:reserved'] = 'Le slug <span class="text-monospace" style="font-size: 95%;">{$a}</span> est réservé.';
$string['error:mod_form:dim:slug:unique'] = 'Les slugs doivent être uniques, renommer ou supprimer les doublons..';
$string['error:mod_form:dim:name:toolong'] = 'Le nom doit comporter 100 caractères au plus.';
$string['error:mod_form:col:missingid'] = 'La dimension <span class="text-monospace" style="font-size: 90%;">identifier</span> doit toujours être incluse.';
$string['error:mod_form:col:notalpha'] = 'Le slug <span class="text-monospace" style="font-size: 90%;">{$a}</span> n\'est pas une chaîne alphanumérique.';
$string['error:mod_form:col:undefined'] = 'La dimension <span class="text-monospace" style="font-size: 90%;">{$a}</span> n\'est pas définie.';
$string['error:mod_form:col:nocols'] = 'Vous devez définir au moins une colonne.';

// Navigation.
$string['navigation:overview'] = "Description";
$string['navigation:leaderboard'] = "Classement";
$string['navigation:lastupdate'] = "Dernière mise à jour: ";

// Summary.
$string['summary:call_to_action'] = "Voir l'activité";
$string['summary:partof'] = "Fait partie de";
$string['summary:individual'] = "Soumissions individuelles";
$string['summary:grouped'] = "Soumissions groupées";
$string['summary:memberof'] = "Vous êtes un membre de";
$string['summary:nogroup'] = "Vous n'appartenez à aucun groupe sur cette activité";
$string['summary:start'] = "Début";
$string['summary:due'] = "Échéance";

// Index page.
$string['index:title'] = "Défis de code";
$string['index:heading'] = "Défis de code";
$string['index:table:id'] = "ID";
$string['index:table:name'] = "Nome";
$string['index:table:created'] = "Date de création";
$string['index:table:view'] = "Voir l'activité";
$string['index:table:empty'] = "Il n'y a pas de cas de défis de code dans ce cours";

// Leaderboard partials.
// Countdown.
$string['leaderboard:countdown:title'] = "Vous êtes toujours en compétition !";
$string['leaderboard:countdown:lead'] = "Vous pourrez voir le classement une fois que le défi sera terminé.";
$string['leaderboard:countdown:availablefrom'] = "Le classement sera disponible à partir de ";

// Empty.
$string['leaderboard:empty:title'] = "Repassez plus tard !";
$string['leaderboard:empty:lead'] = "Il n'y a pas de soumissions à montrer pour le moment, déposez votre travail et revenez plus tard.";
$string['leaderboard:nodata'] = "Aucune donnée";

// List.
$string['leaderboard:list:alternatename'] = "Si votre nom alternatif est fourni, il sera utilisé à la place de votre nom complet. Définissez-le dans la section <b>Noms supplémentaires</b> des paramètres de votre profil.";
$string['leaderboard:list:public'] = "Public";
$string['leaderboard:list:details:view'] = "Voir les détails";
$string['leaderboard:list:details:title'] = "Détails de l'évaluation";
$string['leaderboard:list:group'] = "Groupe";
$string['leaderboard:list:name'] = "Nom";
$string['leaderboard:list:grade'] = "Note";
$string['leaderboard:noncompeting'] = "Vous n'appartenez à aucun groupe de compétition pour accéder à ce classement.";

// Scope selection.
$string['leaderboard:scopeswitch'] = "Changer la portée ...";

// Feedback form.
$string['messages.feedback.call_to_action'] = "Aide &amp; Feedback";
$string['messages.feedback.subtitle'] = "Formulaire de feedback";
$string['messages.feedback.lead'] = "Vous utilisez une version initiale du plugin des défis du code VPL. Certaines fonctionnalités sont encore en cours de développement, tandis que d'autres nécessitent un retour d'information de la part des utilisateurs pour les affiner.";
$string['messages.feedback.indicator.version'] = "Version du plugin installée";
$string['messages.feedback.indicator.external_tool'] = "Outil externe";
$string['messages.feedback.indicator.restricted'] = "Accès restreint";
$string['messages.feedback.form.title'] = "Donnez votre avis";
$string['messages.feedback.form.description'] = "Vos commentaires nous aident à mieux comprendre comment vous utilisez le plugin, afin que nous puissions nous concentrer sur le développement des fonctionnalités qui sont les plus importantes pour les utilisateurs.";
$string['messages.feedback.mattermost.title'] = "Discussion Mattermost";
$string['messages.feedback.mattermost.description'] = "Prenez contact directement avec les développeurs du plugin : donnez votre avis, posez des questions, discutez et suggérez de nouvelles idées de fonctionnalités, ou faites toute autre demande.";
$string['messages.feedback.issues.title'] = "Signaler des bugs";
$string['messages.feedback.issues.description'] = "Si vous rencontrez des bugs ou d'autres problèmes techniques ou fonctionnels lors de l'utilisation du plugin, veuillez les signaler sur la page de développement du plugin.";
$string['messages.feedback.documentation.title'] = "Consulter la documentation";
$string['messages.feedback.documentation.description'] = "Trouvez des guides pratiques, des tutoriels et d'autres détails techniques sur l'utilisation du plugin.";

// Reset form.
$string['messages:reset:all_entries'] = "Supprimer toutes les entrées existantes des classements";
$string['messages:reset:flushed_vplcc'] = 'Suppression des entrées du classement pour {$a}';
$string['messages:reset:could_not_delete'] = 'Impossible de supprimer des entrées';

// Settings.
$string['settings:recordspp'] = "Entrées par page";
$string['settings:recordspp:desc'] = "Les entrées maximales à afficher dans une page du classement.";
$string['settings:feedback'] = "Afficher le lien de feedback";
$string['settings:feedback:desc'] = "Afficher le formulaire de retour d'information et le lien de signalement des bugs sur chaque page d'un défi du code.";
