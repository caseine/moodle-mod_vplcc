<?php
// This file is part of VPL Code Challenges for Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * @author Photis Avrilionis
 * @copyright 2020 Photis Avrilionis
 *
 * This software was developed with the support of the following organizations:
 * - Université Grenoble Alpes
 * - Institut Polytechnique de Grenoble
 */

namespace mod_vplcc\factories;

defined('MOODLE_INTERNAL') || die();

use Exception;
use mod_vplcc\core\database\queries_database_trait;
use mod_vplcc\models\dimension;
use mod_vplcc\models\evaluation;
use mod_vplcc\models\evaluation_fact;
use mod_vplcc\models\facades\vpl_submission;
use mod_vplcc\models\vplcc;

class evaluations_factory {

    use queries_database_trait;

    /**
     * Extracts, transforms and loads leaderboard metadata from all new VPL evaluation scripts.
     *
     * @param vplcc $vplcc
     * @param $vpl
     */
    public static function prepare_leaderboard($vplcc, $vpl) {

        $evaluations = evaluation::all([
            'vplccid' => $vplcc->id,
            'lastupdated' => 0
        ]);

        if ($evaluations !== false) {

            $dimensions = dimension::all([
                'vplccid' => $vplcc->id
            ]);

            $slugs = [];
            foreach ($dimensions as $dimension) {
                $slugs[$dimension->slug] = $dimension->id;
            }

            foreach ($evaluations as $evaluation) {

                $vplsubmission = new vpl_submission($vpl, $evaluation->vplsubmissionid);

                if (isset($vplsubmission->getCE()['executed']) && $vplsubmission->getCE()['executed'] != 0) {

                    // Regular expression details https://regex101.com/r/2ILm5f/1
                    $re = '/^\h*Leaderboard\h*:=>>\h*(?P<key>\w+)\h*>>\h*(?P<value>.*)$/um';
                    preg_match_all($re, $vplsubmission->getce()['execution'], $matches, PREG_SET_ORDER, 0);

                    $transaction = self::db()->start_delegated_transaction();

                    try {
                        foreach ($matches as $match) {
                            if (isset($slugs[$match['key']])) {

                                $criteria = [
                                    'vplccsubmissionid' => $evaluation->id,
                                    'keyid' => $dimensions[$slugs[$match['key']]]->id,
                                ];

                                $metadata = evaluation_fact::find($criteria);

                                if ($metadata == false) {
                                    $metadata = new evaluation_fact($criteria);
                                }

                                $metadata->data = $match['value'];
                                $metadata->save();
                            }
                        }

                        $evaluation->lastupdated = time();
                        $evaluation->save();

                        $transaction->allow_commit();

                    } catch (Exception $e) {
                        $transaction->rollback($e);
                    }
                }
            }
        }
    }

    /**
     * Returns pagination details of a sum of records
     *
     * @param int $totalrecords
     * @param int $pageno
     * @return array
     */
    public static function get_pagination_details($totalrecords, $pageno = 1) {
        $recordsperpage = (int) get_config('mod_vplcc', 'records_per_page');
        return [
            'offset' => ($pageno - 1) * $recordsperpage,
            'current' => $pageno,
            'total_per_page' => $recordsperpage,
            'total_records' => $totalrecords,
            'total_pages' => (int) ceil($totalrecords / $recordsperpage)
        ];
    }

    /**
     * Wraps an SQL where fragment in the query used to select evaluations
     *
     * @param string $fragment
     * @return string
     */
    protected static function wrap_where_fragment($fragment) {
        return "select {vplcc_evaluations}.*, {vpl_submissions}.grade, {vpl_submissions}.dategraded
            from {vplcc_evaluations}
            inner join {vpl_submissions}
                on {vpl_submissions}.id = {vplcc_evaluations}.vplsubmissionid
            where vplccid = ? " . (!empty($fragment) ? "and $fragment" : "") . "
            order by {vpl_submissions}.grade desc, {vpl_submissions}.dategraded";
    }

    /**
     * Wraps an SQL where fragment in the query used to count total evaluations
     *
     * @param string $fragment
     * @return string
     */
    protected static function wrap_where_fragment_count($fragment) {
        return "select count(id) as count from {vplcc_evaluations} where vplccid = ? and $fragment";
    }

    /**
     * Returns a recordset and pagination details for the evaluations selected using a given where fragment with parameters
     *
     * @param vplcc $vplcc
     * @param $fragment
     * @param $params
     * @return array [moodle_recordset|null, $pagination]
     */
    protected static function get_evaluations_by_fragment($vplcc, $fragment, $params, $pageno = 1) {

        $params = array_merge([$vplcc->id], $params);

        $pagination = static::get_pagination_details(self::db()->count_records_sql(static::wrap_where_fragment_count($fragment), $params), $pageno);

        if (($pageno < 1 && $pagination['total_pages'] > 0) || ($pageno > $pagination['total_pages'] && $pagination['total_pages'] > 0)) {
            return [null, $pagination];
        }

        return [self::db()->get_recordset_sql(static::wrap_where_fragment($fragment), $params, $pagination['offset'], $pagination['total_per_page']), $pagination];
    }

    /**
     * Returns a recordset and pagination details for all individual evaluations
     *
     * @param vplcc $vplcc
     * @param int $pageno
     * @return array [moodle_recordset|null, $pagination]
     */
    public static function get_all_individual_evaluations($vplcc, $pageno = 1) {
        return static::get_evaluations_by_fragment($vplcc, '{vplcc_evaluations}.groupid = 0', [], $pageno);
    }

    /**
     * Returns a recordset and pagination details for all group evaluations
     *
     * @param vplcc $vplcc
     * @param int $pageno
     * @return array [moodle_recordset|null, $pagination]
     */
    public static function get_all_group_evaluations($vplcc, $pageno = 1) {
        return static::get_evaluations_by_fragment($vplcc, '{vplcc_evaluations}.userid = 0', [], $pageno);
    }

    /**
     * Returns a recordset and pagination details for all individual evaluations in the same scope as the given user
     *
     * @param vplcc $vplcc
     * @param int $userid
     * @param int $pageno
     * @return array [moodle_recordset|null, $pagination]
     */
    public static function get_individual_evaluations_for_user($vplcc, $userid, $pageno = 1) {

        $users = scopes_factory::get_scope_users_for_user($vplcc, $userid);

        $fragment = self::generate_where_list_fragment('{vplcc_evaluations}.userid', $users);
        $params = array_map(function ($user) {
            return $user->id;
        }, $users);

        return static::get_evaluations_by_fragment($vplcc, $fragment, $params, $pageno);
    }

    /**
     * Returns a recordset and pagination details for all individual evaluations in the given scope
     *
     * @param $vplcc
     * @param $scopeid
     * @param int $pageno
     * @return array [moodle_recordset|null, $pagination]
     */
    public static function get_individual_evaluations_for_scope($vplcc, $scopeid, $pageno = 1) {

        $users = scopes_factory::get_scope_users_for_scope($scopeid);

        $fragment = self::generate_where_list_fragment('{vplcc_evaluations}.userid', $users);
        $params = array_map(function ($user) {
            return $user->id;
        }, $users);

        return static::get_evaluations_by_fragment($vplcc, $fragment, $params, $pageno);
    }

    /**
     * Returns a recordset for and pagination details all group evaluations in the same scope as the given user
     *
     * @param $vplcc
     * @param $vpl
     * @param $userid
     * @param int $pageno
     * @return array [moodle_recordset|null, $pagination]
     */
    public static function get_group_evaluations_for_user($vplcc, $vpl, $userid, $pageno = 1) {

        $groups = scopes_factory::get_scope_groups_for_user($vplcc, $vpl, $userid);

        $fragment = self::generate_where_list_fragment('{vplcc_evaluations}.groupid', $groups);
        $params = array_map(function ($group) {
            return $group->id;
        }, $groups);

        return static::get_evaluations_by_fragment($vplcc, $fragment, $params, $pageno);
    }

    /**
     * Returns a recordset for and pagination details all group evaluations in the given scope
     *
     * @param $vplcc
     * @param $vpl
     * @param $scopeid
     * @param int $pageno
     * @return array [moodle_recordset|null, $pagination]
     */
    public static function get_group_evaluations_for_scope($vplcc, $vpl, $scopeid, $pageno = 1) {

        $groups = scopes_factory::get_scope_groups_for_scope($vplcc, $vpl, $scopeid);

        $fragment = self::generate_where_list_fragment('{vplcc_evaluations}.groupid', $groups);
        $params = array_map(function ($group) {
            return $group->id;
        }, $groups);

        return static::get_evaluations_by_fragment($vplcc, $fragment, $params, $pageno);
    }

    /**
     * Retrieves the latest evaluation update of a code challenge
     *
     * @param vplcc|int $vplcc a vplcc instance or id
     * @return bool
     */
    public static function get_last_update($vplcc) {
        $query = self::db()->get_record_sql(
            'select max(lastupdated) as lastupdate from {vplcc_evaluations} where vplccid = :vplccid group by vplccid', [
                'vplccid' => ($vplcc instanceof vplcc) ? $vplcc->id : $vplcc
            ]
        );
        return ($query !== false) ? $query->lastupdate : false;
    }

    /**
     * Deletes all evaluations associated with a given vplcc
     *
     * @param vplcc $vplcc
     * @return bool
     */
    public static function delete_evaluations($vplcc) {

        $evaluations = evaluation::all([
            'vplccid' => $vplcc->id,
        ]);

        if ($evaluations !== false) {

            $transaction = self::db()->start_delegated_transaction();

            try {
                foreach ($evaluations as $evaluation) {
                    static::delete_evaluation_facts($evaluation);
                    $evaluation->delete();
                }
                $transaction->allow_commit();

            } catch (Exception $e) {
                $transaction->rollback($e);
            }
        }

        return true;
    }

    /**
     * @param evaluation_fact[] $facts
     */
    protected static function delete_facts($facts) {
        $transaction = self::db()->start_delegated_transaction();

        try {
            foreach ($facts as $fact) {
                $fact->delete();
            }

            $transaction->allow_commit();

            return true;
        } catch (Exception $e) {
            $transaction->rollback($e);
        }

        return true;
    }

    public static function delete_dimension_facts(dimension $dimension) {
        $facts = evaluation_fact::all([
            'keyid' => $dimension->id,
        ]);

        if ($facts !== false) {
            return static::delete_facts($facts);
        }

        return true;
    }

    /**
     * Deletes all facts associated with an evaluation
     *
     * @param $evaluation
     * @return bool
     */
    public static function delete_evaluation_facts(evaluation $evaluation) {

        $facts = evaluation_fact::all([
            'vplccsubmissionid' => $evaluation->id,
        ]);

        if ($facts !== false) {
            return static::delete_facts($facts);
        }

        return true;
    }
}
