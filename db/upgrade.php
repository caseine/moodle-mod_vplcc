<?php
// This file is part of VPL Code Challenges for Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * @author Photis Avrilionis
 * @copyright 2020 Photis Avrilionis
 *
 * This software was developed with the support of the following organizations:
 * - Université Grenoble Alpes
 * - Institut Polytechnique de Grenoble
 */

defined('MOODLE_INTERNAL') || die();

function xmldb_vplcc_upgrade($oldversion) {
    global $DB;

    $dbman = $DB->get_manager();

    if ($oldversion < 2020070502) {

        // Table vplcc_vpl_submissions.
        // Define table vplcc_evaluations to be created.
        $vplccevaluationstable = new xmldb_table('vplcc_evaluations');

        // Adding fields to table vplcc_evaluations.
        $vplccevaluationstable->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $vplccevaluationstable->add_field('vplccid', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, null);
        $vplccevaluationstable->add_field('vplsubmissionid', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, null);
        $vplccevaluationstable->add_field('userid', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, null);
        $vplccevaluationstable->add_field('groupid', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, null);
        $vplccevaluationstable->add_field('lastupdated', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, null);

        // Adding keys to table vplcc_evaluations.
        $vplccevaluationstable->add_key('primary', XMLDB_KEY_PRIMARY, ['id']);
        $vplccevaluationstable->add_key('fk_vplcc_evaluations__vplcc_idx', XMLDB_KEY_FOREIGN, ['vplccid'], 'vplcc', ['id']);
        $vplccevaluationstable->add_key('fk_vplcc_evaluations__user_idx', XMLDB_KEY_FOREIGN, ['userid'], 'user', ['id']);
        $vplccevaluationstable->add_key('fk_vplcc_evaluations__groups_idx', XMLDB_KEY_FOREIGN, ['groupid'], 'groups', ['id']);
        $vplccevaluationstable->add_key('fk_vplcc_evaluations__vpl_submissions_idx', XMLDB_KEY_FOREIGN, ['vplsubmissionid'], 'vpl_submissions', ['id']);

        // Table vplcc_leaderboard_keys.
        // Define table vplcc_dimensions to be created.
        $vplccdimensionstable = new xmldb_table('vplcc_dimensions');

        // Adding fields to table vplcc_dimensions.
        $vplccdimensionstable->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $vplccdimensionstable->add_field('vplccid', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, null);
        $vplccdimensionstable->add_field('slug', XMLDB_TYPE_CHAR, '50', null, XMLDB_NOTNULL, null, null);
        $vplccdimensionstable->add_field('name', XMLDB_TYPE_CHAR, '100', null, null, null, null);
        $vplccdimensionstable->add_field('description', XMLDB_TYPE_TEXT, null, null, null, null, null);

        // Adding keys to table vplcc_dimensions.
        $vplccdimensionstable->add_key('primary', XMLDB_KEY_PRIMARY, ['id']);
        $vplccdimensionstable->add_key('fk_vplcc_dimensions__vplcc_idx', XMLDB_KEY_FOREIGN, ['vplccid'], 'vplcc', ['id']);

        // Table vplcc_vpl_submission_metadata.
        // Define table vplcc_submission_metadata to be created.
        $vplccevaluationfactstable = new xmldb_table('vplcc_evaluation_facts');

        // Adding fields to table vplcc_evaluation_facts.
        $vplccevaluationfactstable->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $vplccevaluationfactstable->add_field('vplccsubmissionid', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, null);
        $vplccevaluationfactstable->add_field('keyid', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, null);
        $vplccevaluationfactstable->add_field('data', XMLDB_TYPE_TEXT, null, null, null, null, null);

        // Adding keys to table vplcc_evaluation_facts.
        $vplccevaluationfactstable->add_key('primary', XMLDB_KEY_PRIMARY, ['id']);
        $vplccevaluationfactstable->add_key('fk_vplcc_evaluation_facts__vplcc_evaluations_idx', XMLDB_KEY_FOREIGN, ['vplccsubmissionid'], 'vplcc_submissions', ['id']);
        $vplccevaluationfactstable->add_key('fk_vplcc_evaluation_facts__vplcc_dimensions_idx', XMLDB_KEY_FOREIGN, ['keyid'], 'vplcc_leaderboard_keys', ['id']);

        // Start database transaction
        $transaction = $DB->start_delegated_transaction();

        try {

            // Conditionally launch create table for vplcc_evaluations.
            if (!$dbman->table_exists($vplccevaluationstable)) {
                $dbman->create_table($vplccevaluationstable);
            }

            // Conditionally launch create table for vplcc_dimensions.
            if (!$dbman->table_exists($vplccdimensionstable)) {
                $dbman->create_table($vplccdimensionstable);
            }

            // Conditionally launch create table for vplcc_evaluation_facts.
            if (!$dbman->table_exists($vplccevaluationfactstable)) {
                $dbman->create_table($vplccevaluationfactstable);
            }

            // Commit transaction.
            $transaction->allow_commit();

            // Vplcc savepoint reached.
            upgrade_mod_savepoint(true, 2020070502, 'vplcc');

        } catch (Exception $e) {

            // Rollback transaction if updates fail.
            $transaction->rollback($e);
        }
    }

    if ($oldversion < 2020071010) {

        // Define fields columns and metadata to be added to vplcc.
        $table = new xmldb_table('vplcc');
        $column = new xmldb_field('columns', XMLDB_TYPE_TEXT, null, null, null, null, null, 'vplcm');
        $metadata = new xmldb_field('metadata', XMLDB_TYPE_TEXT, null, null, null, null, null, 'columns');

        // Start database transaction
        $transaction = $DB->start_delegated_transaction();

        try {

            // Conditionally launch add field columns.
            if (!$dbman->field_exists($table, $column)) {
                $dbman->add_field($table, $column);

                // Migrate existing vplcc activities.
                $DB->execute("UPDATE {vplcc} SET {vplcc}.columns = 'identifier,grade'");
            }

            // Conditionally launch add field metadata.
            if (!$dbman->field_exists($table, $metadata)) {
                $dbman->add_field($table, $metadata);
            }

            $transaction->allow_commit();

            // Vplcc savepoint reached.
            upgrade_mod_savepoint(true, 2020071010, 'vplcc');

        } catch (Exception $e) {

            // Rollback transaction if updates fail.
            $transaction->rollback($e);
        }
    }

    /*
     * Adds the course column to vplcc table
     * Addresses issue #21 (course column missing from install.xml)
     */
    if ($oldversion < 2020071100) {

        // Define column course to be added to vplcc.
        $table = new xmldb_table('vplcc');
        $column = new xmldb_field('course', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, null, 'id');
        $key = new xmldb_key('fk_vplcc_course', XMLDB_KEY_FOREIGN, ['course'], 'course', ['id']);

        // Start database transaction
        $transaction = $DB->start_delegated_transaction();

        try {

            // Conditionally launch add column course.
            if (!$dbman->field_exists($table, $column)) {
                $dbman->add_field($table, $column);
                $dbman->add_key($table, $key);
            }

            // Migrate existing vplcc activities.
            $sql = "UPDATE {vplcc}
                INNER JOIN {course_modules} cm ON cm.instance = {vplcc}.id
                SET {vplcc}.course = cm.course";
            $DB->execute($sql);

            // Commit transaction.
            $transaction->allow_commit();

            // Vplcc savepoint reached.
            upgrade_mod_savepoint(true, 2020071100, 'vplcc');

        } catch (Exception $e) {

            // Rollback transaction if updates fail.
            $transaction->rollback($e);
        }
    }

    /*
     * Sets default values for new mod_vplcc configuration settings
     */
    if ($oldversion < 2020071200) {
        set_config('records_per_page', 30, 'mod_vplcc');
        set_config('show_feedback', 1, 'mod_vplcc');

        upgrade_mod_savepoint(true, 2020071200, 'vplcc');
    }

    /*
     * Adds scope field to vplcc
     */
    if ($oldversion < 2020072202) {

        // Define field scope to be added to vplcc.
        $table = new xmldb_table('vplcc');
        $field = new xmldb_field('scope', XMLDB_TYPE_INTEGER, '10', null, null, null, '0', 'vplcm');

        // Conditionally launch add field scope.
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }

        // Vplcc savepoint reached.
        upgrade_mod_savepoint(true, 2020072202, 'vplcc');
    }

    return true;
}
